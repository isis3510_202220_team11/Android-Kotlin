package com.example.myapplication.activities


import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.helper.DatabaseHelperPromo
import com.example.myapplication.util.ConnectionLiveData

class LocalStoragePromos : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView

    lateinit var connectionLiveData: ConnectionLiveData

    var pruebinha: CustomAdapterPromos? = null
    var myDB: DatabaseHelperPromo? = null
    var promo_name: ArrayList<String>? = null
    var promo_before: ArrayList<String>? = null
    var promo_after: ArrayList<String>? = null
    var promo_img: ArrayList<String>? = null
    var promo_description: ArrayList<String>? = null
    var promo_place: ArrayList<String>? = null
    var promo_dicount: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){
                val intent = Intent(this, PromosActivity::class.java)
                startActivity(intent)
            }
            else{

            }

        }
        setContentView(R.layout.activity_no_internet_promos)
        recyclerView = findViewById(R.id.vistaspromos1)

        myDB = DatabaseHelperPromo(this@LocalStoragePromos)
        promo_name = ArrayList()
        promo_before = ArrayList()
        promo_after = ArrayList()
        promo_img = ArrayList()
        promo_description = ArrayList()
        promo_place = ArrayList()
        promo_dicount = ArrayList()
        storeDataInArrays()
        pruebinha = CustomAdapterPromos(
            this@LocalStoragePromos,
            promo_name!!,
            promo_before!!,
            promo_after!!,
            promo_img!!,
            promo_description!!,
            promo_place!!,
            promo_dicount!!
        )
        recyclerView.adapter = pruebinha
        recyclerView.layoutManager = LinearLayoutManager(this@LocalStoragePromos)



    }


    fun storeDataInArrays() {
        myDB!!.addPromo()
        val cursor = myDB!!.readAllData()
        if (cursor != null) {
            if (cursor.count == 0) {
                Toast.makeText(this, "No data.", Toast.LENGTH_SHORT).show()
            } else {
                while (cursor.moveToNext()) {
                    promo_name!!.add(cursor.getString(0))
                    promo_before!!.add(cursor.getString(1))
                    promo_after!!.add(cursor.getString(2))
                    promo_img!!.add(cursor.getString(3))
                    promo_description!!.add(cursor.getString(4))
                    promo_place!!.add(cursor.getString(5))
                    promo_dicount!!.add(cursor.getString(6))
                }
            }
        }
    }
}