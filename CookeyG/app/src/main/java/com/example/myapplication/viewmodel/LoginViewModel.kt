package com.example.myapplication.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.util.Resource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val firebaseAuth: FirebaseAuth, application: Application
): AndroidViewModel(application) {
        private val _login = MutableSharedFlow<Resource<FirebaseUser>>()
    val login = _login.asSharedFlow()
    private val context = getApplication<Application>().applicationContext
    private val sharedPreference: SharedPreferences =  context.getSharedPreferences("USER_INFO",Context.MODE_PRIVATE)

    private val _resetPassword = MutableSharedFlow<Resource<String>>()
    val resetPassword = _resetPassword.asSharedFlow()


    fun login(email: String, password: String){
        var userUID=""
        viewModelScope.launch { _login.emit(Resource.Loading()) }
        firebaseAuth.signInWithEmailAndPassword(
            email, password
        ).addOnSuccessListener {
            viewModelScope.launch {
                it.user?.let {
                    userUID=it.uid
                    _login.emit(Resource.Succes(it))
                }

            }
            with(sharedPreference.edit()){
                putString("email", email)
                putString("UID", userUID)
                apply()
            }
        }.addOnFailureListener{
            viewModelScope.launch {
                _login.emit(Resource.Error(it.message.toString()))
            }
        }
    }
    fun resetPassword(email: String)
    {
        viewModelScope.launch {
            _resetPassword.emit(Resource.Loading())
        }
            firebaseAuth
                .sendPasswordResetEmail(email)
                .addOnSuccessListener {
                    viewModelScope.launch {
                        _resetPassword.emit(Resource.Succes(email))
                        val sharedPreference = getApplication<Application>().applicationContext.getSharedPreferences("USER_DATA",Context.MODE_PRIVATE)
                    }
                }.addOnFailureListener{
                    viewModelScope.launch {
                        _resetPassword.emit(Resource.Error(it.message.toString()))

                    }
                }
        }

    }
