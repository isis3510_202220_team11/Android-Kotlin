package com.example.myapplication.adapters

import com.example.myapplication.data.BarcodeResponse
import com.example.myapplication.data.ProductResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface BarcodeAPI {
    @Multipart
    @POST("/readBarcode")
    suspend fun uploadImage(
        @Part id : MultipartBody.Part,
        @Part image : MultipartBody.Part
    ):Response<BarcodeResponse>

    @GET("/getBarcodeInfo")
    suspend fun getInfo(
        @Query("barcode") barcode:String
    ): Response<ProductResponse>

    companion object{
        val instace by lazy {
            Retrofit.Builder()
                .baseUrl("https://barcode-reader-cookey.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BarcodeAPI::class.java)
        }
    }

}