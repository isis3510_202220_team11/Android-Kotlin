package com.example.myapplication.adapters

import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.IngredientUser
import com.example.myapplication.data.UnitsEnum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class InventoryAdapter (val itemList : ArrayList<IngredientUser>, private val inventoryRepository: InventoryRepository)
    : RecyclerView.Adapter<InventoryAdapter.MyViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.inventory_item_card,
            parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item : IngredientUser = itemList[position]
        Log.d("Bind item", item.name)
        holder.name.text = item.name
        GlobalScope.launch (Dispatchers.IO){
            getImage(item.img, holder)
        }
        var quantity = item.quantity.toString()+" "
        quantity+=when(item.unit){
            UnitsEnum.UNIT->"units"
            UnitsEnum.GRAM->"grams"
            UnitsEnum.LITER->"liters"
            else->""
        }
        holder.quantity.text = quantity
        holder.delete_btn.setOnClickListener{
            deleteItem(item)
        }
    }

    private suspend fun getImage(imgURL: String ?= null, holder: MyViewHolder){
        try{
                val url = URL(imgURL)
                val img1 = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                withContext(Dispatchers.Main){
                    holder.img.setImageBitmap(img1)
                }
            }
            catch(e: Exception){
                withContext(Dispatchers.Main){
                    holder.img.setImageResource(R.drawable.noavailable)
                }
            }
    }

    private fun deleteItem(itemId:IngredientUser){
        GlobalScope.launch(Dispatchers.IO) {
            inventoryRepository.deleteItem(itemId, this@InventoryAdapter)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int){
        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    public class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val name : TextView = itemView.findViewById(R.id.inventory_item_name)
        val img : ImageView = itemView.findViewById(R.id.item_image)
        val quantity : TextView = itemView.findViewById(R.id.inventory_item_quantity)
        val delete_btn : ImageButton = itemView.findViewById(R.id.delete_item_icon)
    }


}