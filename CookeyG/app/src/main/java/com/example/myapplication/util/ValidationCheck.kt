package com.example.myapplication.util

import android.util.Patterns

fun validateEmail (email: String): RegisterValidation{
    if (email.isEmpty())
        return RegisterValidation.Failed("Email cannot be empty"
        )
    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        return RegisterValidation.Failed("Wrong email format")

    if (email.length> 50)
        return RegisterValidation.Failed("Email should contain less than 50 characters")

    return RegisterValidation.Succes
}

fun validateLastName(lastName: String): RegisterValidation{
    if (lastName.isEmpty())
        return RegisterValidation.Failed("Last Name Cannot be empty")

    return RegisterValidation.Succes
}

fun validateCountry (country: String): RegisterValidation{
    if (country.isEmpty())
        return RegisterValidation.Failed("Country Cannot be empty")

    return RegisterValidation.Succes
}

fun validateFirstName (firstName: String): RegisterValidation{
    if (firstName.isEmpty())
        return RegisterValidation.Failed("First name cannot be empty"
        )
    if (firstName.length> 30)
        return RegisterValidation.Failed("First name should containn less than 30 characters")

    return RegisterValidation.Succes
}

fun validatePassword(password: String): RegisterValidation{
    if (password.isEmpty())
        return RegisterValidation.Failed("Password Cannot be empty")

    if (password.length< 6)
        return RegisterValidation.Failed("Password should contain 6 characters")

    return RegisterValidation.Succes
}