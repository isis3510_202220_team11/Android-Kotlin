package com.example.myapplication.adapters

import android.util.Log
import com.example.myapplication.data.BarcodeResponse
import com.example.myapplication.data.ProductResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import java.io.File

class SearchBarcode {

    suspend fun searchBarcodeByPhoto(file : File) : ProductResponse? {
        val decode = findBarcodeOnImg(file)
        if (decode === null) {
            return null
        } else {
            //return findInfoFromBarcode(decode!!.barcode)?.title
            return findInfoFromBarcode(decode.barcode)
        }
    }

    private suspend fun findBarcodeOnImg(file: File) : BarcodeResponse?{
        try{
            return BarcodeAPI.instace.uploadImage(
                id=MultipartBody.Part
                    .createFormData("id", "1"),
                image =MultipartBody.Part
                    .createFormData("img", file.name, file.asRequestBody())
            ).body()
        }
        catch (e: HttpException){
            Log.e("HTTPError",e.message())
            return null
        }
    }

    suspend fun findInfoFromBarcode(barcode:String):ProductResponse?{
        try{
            return BarcodeAPI.instace.getInfo(barcode).body()
        }
        catch (e: HttpException){
            e.printStackTrace()
            return ProductResponse("","Error","")

        }
    }
}