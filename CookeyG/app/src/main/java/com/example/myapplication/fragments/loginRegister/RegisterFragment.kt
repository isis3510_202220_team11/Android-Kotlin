package com.example.myapplication.fragments.loginRegister

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.data.User
import com.example.myapplication.databinding.FragmentRegisterBinding
import com.example.myapplication.util.ConnectionLiveData
import com.example.myapplication.util.RegisterValidation
import com.example.myapplication.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private val TAG = "RegisterFragment"
@AndroidEntryPoint
class RegisterFragment: Fragment() {
    lateinit var connectionLiveData: ConnectionLiveData
    private lateinit var binding: FragmentRegisterBinding
    private val viewModel by viewModels<RegisterViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater)
        connectionLiveData = context?.let { ConnectionLiveData(it) }!!
        connectionLiveData.observe(viewLifecycleOwner) {networkAvailable->
            if(networkAvailable){

                binding.internetLayout.visibility = VISIBLE
                binding.noInternetLayout.visibility = GONE
            }
            else{

                binding.noInternetLayout.visibility = VISIBLE
                binding.internetLayout.visibility = GONE
            }

        }
        drawLayout()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonBack.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_introductionFragment)
        }

        binding.apply {
            buttonsignup.setOnClickListener {
                val user = User(
                    username2.text.toString().trim(),
                    editTextTextPersonName3.text.toString().trim(),
                    emailRegister.text.toString().trim(),
                    country.text.toString().trim()
                )
                val password = password2.text.toString()
                viewModel.createAccountWithEmailAndPassword(user,password)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.register.collect{
                when(it){
                    is com.example.myapplication.util.Resource.Loading -> {
                        binding.buttonsignup.startAnimation()
                    }
                    is com.example.myapplication.util.Resource.Succes -> {
                        Log.d("test", it.data.toString())
                        binding.buttonsignup.revertAnimation()
                    }
                    is com.example.myapplication.util.Resource.Error -> {
                        Log.e(TAG, it.message.toString())
                        binding.buttonsignup.revertAnimation()
                    }
                    else->Unit

                }
            }


        }

        lifecycleScope.launchWhenStarted {
            viewModel.validation.collect{ validation ->

                if (validation.firstName is RegisterValidation.Failed){
                    withContext(Dispatchers.Main){
                        binding.username2.apply {
                            requestFocus()
                            error = validation.firstName.message
                        }
                    }
                }

                if (validation.lastName is RegisterValidation.Failed){
                    withContext(Dispatchers.Main){
                        binding.editTextTextPersonName3.apply {
                            requestFocus()
                            error = validation.lastName.message
                        }
                    }
                }
                if (validation.email is RegisterValidation.Failed){
                    withContext(Dispatchers.Main){
                        binding.emailRegister.apply {
                            requestFocus()
                            error = validation.email.message
                        }
                    }
                }
                if (validation.country is RegisterValidation.Failed){
                    withContext(Dispatchers.Main){
                        binding.country.apply {
                            requestFocus()
                            error = validation.country.message
                        }
                    }
                }
                if (validation.password is RegisterValidation.Failed){
                    withContext(Dispatchers.Main){
                        binding.password2.apply {
                            requestFocus()
                            error = validation.password.message
                        }
                    }
                }
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val cm = getActivity()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }

    private fun drawLayout() {
        if (isNetworkAvailable()) {
            binding.internetLayout.visibility = VISIBLE
            binding.noInternetLayout.visibility = GONE
        } else {
            binding.noInternetLayout.visibility = VISIBLE
            binding.internetLayout.visibility = GONE
        }
    }
}