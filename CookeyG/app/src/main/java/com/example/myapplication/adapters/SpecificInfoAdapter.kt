package com.example.myapplication.adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.UserRecipe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class SpecificInfoAdapter(private val userList : ArrayList<UserRecipe> ) : RecyclerView.Adapter<SpecificInfoAdapter.MyViewHolder>() {

    //-----------------------------------

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int){

        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpecificInfoAdapter.MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_specific_info,
        parent, false)

        return MyViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: SpecificInfoAdapter.MyViewHolder, position: Int) {


        val userRecipe : UserRecipe = userList[position]
        holder.name.text = userRecipe.name
        GlobalScope.launch (Dispatchers.IO){
            getImage(userRecipe.img, holder)
        }
        holder.calories.text = userRecipe.calories.toString()
        holder.recipe.text = userRecipe.recipe
        holder.likedBy.text = userRecipe.likedBy.toString()
        holder.type.text = userRecipe.type.toString()

    }

    private suspend fun getImage(imgURL: String ?= null, holder: MyViewHolder){
        val url = URL(imgURL)
        val img = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        withContext(Dispatchers.Main){
            holder.img.setImageBitmap(img)
        }

    }

    override fun getItemCount(): Int {

        return userList.size

    }

    public class MyViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){

        val name : TextView = itemView.findViewById(R.id.recipeName3)
        val img : ImageView = itemView.findViewById(R.id.recipesImage3)
        val calories : TextView = itemView.findViewById(R.id.recipeCalories3)
        val recipe : TextView = itemView.findViewById(R.id.descriptionText3)
        val likedBy : TextView = itemView.findViewById(R.id.recipeLikedBy3)
        val type : TextView = itemView.findViewById(R.id.recipeType3)


        //-----------------------------------
        init{
            itemView.setOnClickListener {

                listener.onItemClick(adapterPosition)

            }
        }
    }
}