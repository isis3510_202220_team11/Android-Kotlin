package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.MyAdapter3
import com.example.myapplication.data.User
import com.example.myapplication.data.UserRecipe
import com.example.myapplication.util.ConnectionLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*

class RecommendationsActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var userArrayList: ArrayList<UserRecipe>
    private lateinit var myAdapter3: MyAdapter3
    private lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    private lateinit var user: User
    private lateinit var uid: String
    private lateinit var email: User
    private lateinit var country: String

    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){
            }
            else{
                val intent = Intent(this, NoInternetRecommendations::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.activity_recommendations)

        val actionBar = supportActionBar
        actionBar!!.title = "Recommendations"

        actionBar.setDisplayHomeAsUpEnabled(true)


        recyclerView = findViewById(R.id.viewRecommendations)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        userArrayList = arrayListOf()

        var adapter = MyAdapter3(userArrayList)
        myAdapter3 = adapter
        adapter.setOnItemClickListener(object : MyAdapter3.onItemClickListener{
            override fun onItemClick(position: Int) {
                val intent = Intent(this@RecommendationsActivity, SpecificInfoMenuActivity2::class.java)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("calories", userArrayList[position].calories.toString())
                intent.putExtra("country", userArrayList[position].country)
                intent.putExtra("likedBy", userArrayList[position].likedBy.toString())
                intent.putExtra("recipe", userArrayList[position].recipe)
                intent.putExtra("img", userArrayList[position].img)
                intent.putExtra("type", userArrayList[position].type   )
                startActivity(intent)

            }
        })

        recyclerView.adapter = myAdapter3

        EventChangeListener()

    }

    private fun EventChangeListener() {

        db = FirebaseFirestore.getInstance()

        auth = FirebaseAuth.getInstance()

        //uid = auth.currentUser?.email.toString()
        // var countr: String? =""
        db.collection("user").document(FirebaseAuth.getInstance().currentUser!!.uid)
            .get().addOnCompleteListener { task: Task<DocumentSnapshot?> ->
                if (task.isSuccessful && task.result != null) {
                    val email = task.result!!.getString("email")
                    val countr = task.result!!.getString("country")
                    db.collection("Recipes").whereEqualTo("country", countr)

                        .addSnapshotListener(object : EventListener<QuerySnapshot> {
                            override fun onEvent(
                                value: QuerySnapshot?,
                                error: FirebaseFirestoreException?
                            ) {

                                if (error != null) {

                                    Log.e("Firestore Error", error.message.toString())
                                    return

                                }

                                for (dc: DocumentChange in value?.documentChanges!!) {
                                    if (dc.type == DocumentChange.Type.ADDED) {
                                        userArrayList.add(dc.document.toObject(UserRecipe::class.java))
                                    }
                                }

                                myAdapter3.notifyDataSetChanged()

                            }

                        })
                    //other stuff
                } else {
                    //deal with error
                }
            }


    }

    private fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }

}


