package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.BestDealsAdapter
import com.example.myapplication.data.Promos
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.firestore.*

class BestDealsActivity : AppCompatActivity()  {
    private lateinit var recyclerView : RecyclerView
    private lateinit var userArrayList : ArrayList<Promos>
    private lateinit var myAdapter : BestDealsAdapter
    private lateinit var db: FirebaseFirestore

    private lateinit var internetLayout: RelativeLayout
    private lateinit var noInternetLayout: RelativeLayout

    lateinit var connectionLiveData: ConnectionLiveData
    private lateinit var tryAgainButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){
            }
            else{
                val intent = Intent(this, LocalStorageBestDeals::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.activity_best_deals)

        val actionBar = supportActionBar
        actionBar!!.title = "Promos"


        actionBar.setDisplayHomeAsUpEnabled(true)

        recyclerView = findViewById(R.id.vistaspromos2)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        userArrayList = arrayListOf()

        //------------------------------

        var adapter = BestDealsAdapter(userArrayList)
        myAdapter = adapter
        adapter.setOnItemClickListener(object : BestDealsAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                val intent = Intent(this@BestDealsActivity, SpecificInfoPromosActivity::class.java)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("img", userArrayList[position].img)
                intent.putExtra("before", userArrayList[position].before)
                intent.putExtra("after", userArrayList[position].after)
                intent.putExtra("place", userArrayList[position].place)
                intent.putExtra("discount", userArrayList[position].discount.toString())
                intent.putExtra("description", userArrayList[position].description)
                startActivity(intent)

            }
        })

        recyclerView.adapter = myAdapter

        EventChangeListener()

    }

    private fun EventChangeListener() {

        db = FirebaseFirestore.getInstance()
        //db.collection("Recipes").orderBy("name", Query.Direction.ASCENDING)
        db.collection("promos").orderBy("discount", Query.Direction.DESCENDING).limit(5)
            .
            addSnapshotListener(object : EventListener<QuerySnapshot>
            {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {

                    if (error != null){

                        Log.e("Firestore Error", error.message.toString())
                        return

                    }

                    for(dc : DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            userArrayList.add(dc.document.toObject(Promos::class.java))
                        }
                    }

                    myAdapter.notifyDataSetChanged()

                }

            })


    }

    private fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))

    }

    private fun drawLayout() {
        if (isNetworkAvailable()) {
            internetLayout.visibility = View.VISIBLE
            noInternetLayout.visibility = View.GONE
        } else {
            noInternetLayout.visibility = View.VISIBLE
            internetLayout.visibility = View.GONE
        }
    }
}