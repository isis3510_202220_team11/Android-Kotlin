package com.example.myapplication.data

data class UserRecipe2(var name: String ?= null, var img: String ?= null, var calories: Int ?= null, var recipe: String ?= null, var likedBy: Int ?= null, var type: String ?= null, var country: String?= null)
