package com.example.myapplication.adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.Promos
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class SpecificInfoPromosAdapter(private val promoList : ArrayList<Promos> ) : RecyclerView.Adapter<SpecificInfoPromosAdapter.MyViewHolder>() {

    //-----------------------------------

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int){

        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpecificInfoPromosAdapter.MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.activity_specific_promos,
            parent, false)

        return MyViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: SpecificInfoPromosAdapter.MyViewHolder, position: Int) {


        val userRecipe : Promos = promoList[position]
        holder.name.text = userRecipe.name
        GlobalScope.launch (Dispatchers.IO){
            getImage(userRecipe.img, holder)
        }
        holder.before.text = userRecipe.before
        holder.after.text = userRecipe.after
        holder.place.text = userRecipe.place
        holder.discount.text = userRecipe.discount.toString()
        holder.description.text = userRecipe.description

    }

    private suspend fun getImage(imgURL: String ?= null, holder: MyViewHolder){
        val url = URL(imgURL)
        val img = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        withContext(Dispatchers.Main){
            holder.img.setImageBitmap(img)
        }

    }

    override fun getItemCount(): Int {

        return promoList.size

    }

    public class MyViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){

        val name : TextView = itemView.findViewById(R.id.product_names)
        val img : ImageView = itemView.findViewById(R.id.img_products)
        val before : TextView = itemView.findViewById(R.id.beforetexts)
        val after : TextView = itemView.findViewById(R.id.aftertexts)
        val place : TextView = itemView.findViewById(R.id.places)
        val discount : TextView = itemView.findViewById(R.id.discounts)
        val description : TextView = itemView.findViewById(R.id.descriptions)
        //-----------------------------------
        init{
            itemView.setOnClickListener {

                listener.onItemClick(adapterPosition)

            }
        }
    }
}