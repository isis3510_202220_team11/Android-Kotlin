package com.example.myapplication.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R

class NoInternetRecipes : AppCompatActivity()   {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipes_no_connection)

        val actionBar = supportActionBar
        actionBar!!.title = "Recipes"

        actionBar.setDisplayHomeAsUpEnabled(true)




    }
}