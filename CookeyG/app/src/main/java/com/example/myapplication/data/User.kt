package com.example.myapplication.data

data class User(
    val firstName: String,
    val lastName: String,
    val email: String,
    val country: String,
){
    constructor(): this("","","", "")
}
