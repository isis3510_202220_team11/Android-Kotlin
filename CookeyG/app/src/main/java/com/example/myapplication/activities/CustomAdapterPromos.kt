package com.example.myapplication.activities

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.util.CachePromos
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class CustomAdapterPromos(private val context: Context,
                          private val promo_name: ArrayList<*>,
                          private val promo_after: ArrayList<*>,
                          private val promo_before: ArrayList<*>, //private Activity activity;
                          private val promo_img: ArrayList<*>,
                          private val promo_description: ArrayList<*>,
                          private val promo_place: ArrayList<*>,
                          private val promo_dicount: ArrayList<*>) : RecyclerView.Adapter<CustomAdapterPromos.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterPromos.MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.no_internet_promos, parent, false)
        return MyViewHolder(view)
    }

    @RequiresApi(api = Build.VERSION_CODES.M) // @Override
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.promo_name_txt.text = promo_name[position].toString()
        holder.promo_before_txt.text = promo_before[position].toString()
        holder.promo_after_txt.text = promo_after[position].toString()
        GlobalScope.launch (Dispatchers.IO){
            getImage(promo_img[position].toString(), holder)
        }
        holder.promo_dicount_txt.text = promo_dicount[position].toString()
       // holder.promo_description_txt.text = promo_description[position].toString()
      //  holder.promo_place_txt.text = promo_place[position].toString()

    }
    private suspend fun getImage(imgURL: String ?= null, holder: CustomAdapterPromos.MyViewHolder){
        var img1 = CachePromos.instance.retrieveBitmapFromCache(imgURL!!)
        if(img1 != null){
            withContext(Dispatchers.Main){
                holder.img_url.setImageBitmap(img1)
            }
        }
        else{
            try{
                val url = URL(imgURL)
                img1 = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                CachePromos.instance.saveBitmapToCahche(imgURL, img1)
                withContext(Dispatchers.Main){
                    holder.img_url.setImageBitmap(img1)
                }
            }
            catch(e: Exception){
                withContext(Dispatchers.Main){
                    holder.img_url.setImageResource(R.drawable.noavailable)
                }
            }

        }

    }
    override fun getItemCount(): Int {
        return promo_name.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var promo_name_txt: TextView
        var promo_img: TextView? = null
        var promo_before_txt: TextView
        var promo_after_txt: TextView
        var promo_dicount_txt: TextView
        //var promo_description_txt: TextView? = null

       // var promo_place_txt: TextView
        var img_url: ImageView

        init {
            promo_name_txt = itemView.findViewById(R.id.product_name1)
            promo_before_txt = itemView.findViewById(R.id.beforetext1)
            promo_after_txt = itemView.findViewById(R.id.aftertext1)
            img_url = itemView.findViewById(R.id.img_product1)
            //promo_place_txt = itemView.findViewById(R.id.place1)
            promo_dicount_txt = itemView.findViewById(R.id.discount1)
           // promo_description_txt = itemView.findViewById(R.id.aftertext1)
        }
    }



}