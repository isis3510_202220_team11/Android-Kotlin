package com.example.myapplication.activities

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.util.MyCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class CustomAdapterLocalStorage(private val context: Context,
                                private val recipe_name: ArrayList<*>,
                                private val recipe_calories: ArrayList<*>, //private Activity activity;
                                private val recipe_country: ArrayList<*>,
                                private val recipe_img: ArrayList<*>,
                                private val recipe_recipe: ArrayList<*>,
                                private val recipe_type: ArrayList<*>,
                                private val recipe_likedBy: ArrayList<*>) : RecyclerView.Adapter<CustomAdapterLocalStorage.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.no_internet, parent, false)
        return MyViewHolder(view)
    }

    @RequiresApi(api = Build.VERSION_CODES.M) // @Override
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //holder.recipe_country_txt.setText(String.valueOf(recipe_country.get(position)));
        //holder.recipe_img_txt.setText(String.valueOf(recipe_img.get(position)));
        holder.recipe_name_txt.text = recipe_name[position].toString()
        GlobalScope.launch (Dispatchers.IO){
            getImage(recipe_img[position].toString(), holder)
        }
        //holder.recipe_recipe_txt.setText(String.valueOf(recipe_recipe.get(position)));
        holder.recipe_type_txt.text = recipe_type[position].toString()
        holder.recipe_calories_txt.text = recipe_calories[position].toString()
        holder.recipe_likedBy_txt.text = recipe_likedBy[position].toString()


        //Recyclerview onClickListener
        //holder.mainLayout.setOnClickListener(new View.OnClickListener() {
        //  @Override
        //public void onClick(View view) {
        //  Intent intent = new Intent(context, UpdateActivity.class);
        //intent.putExtra("id", String.valueOf(book_id.get(position)));
        //intent.putExtra("title", String.valueOf(book_title.get(position)));
        //intent.putExtra("author", String.valueOf(book_author.get(position)));
        //intent.putExtra("pages", String.valueOf(book_pages.get(position)));
        //activity.startActivityForResult(intent, 1);
    }
    private suspend fun getImage(imgURL: String ?= null, holder: CustomAdapterLocalStorage.MyViewHolder){
        var img1 = MyCache.instance.retrieveBitmapFromCache(imgURL!!)
        if(img1 != null){
            withContext(Dispatchers.Main){
                holder.img_url.setImageBitmap(img1)
            }
        }
        else{
            try{
                val url = URL(imgURL)
                img1 = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                MyCache.instance.saveBitmapToCahche(imgURL, img1)
                withContext(Dispatchers.Main){
                    holder.img_url.setImageBitmap(img1)
                }
            }
            catch(e: Exception){
                //Toast.makeText(this.context.applicationContext,"Your like has been sent", Toast.LENGTH_SHORT).show()
                withContext(Dispatchers.Main){
                    holder.img_url.setImageResource(R.drawable.noavailable)
                }
            }

        }

    }

    override fun getItemCount(): Int {
        return recipe_name.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var recipe_country_txt: TextView? = null
        var recipe_img_txt: TextView? = null
        var recipe_name_txt: TextView
        var recipe_recipe_txt: TextView? = null
        var recipe_type_txt: TextView
        var recipe_calories_txt: TextView
        var recipe_likedBy_txt: TextView
        var img_url: ImageView

        //LinearLayout mainLayout;
        init {
            //recipe_country_txt = itemView.findViewById(R.id.recipeName33);
            //recipe_img_txt = itemView.findViewById(R.id.recipesImage33);
            img_url = itemView.findViewById(R.id.recipesImage33)
            recipe_name_txt = itemView.findViewById(R.id.recipeName33)
            //recipe_recipe_txt = itemView.findViewById(R.id.recipeName33);
            recipe_type_txt = itemView.findViewById(R.id.recipeType33)
            recipe_calories_txt = itemView.findViewById(R.id.recipeCalories33)
            recipe_likedBy_txt = itemView.findViewById(R.id.recipeLikedBy33)
            //mainLayout = itemView.findViewById(R.id.mainLayout);
            //Animate Recyclerview
            //Animation translate_anim = AnimationUtils.loadAnimation(context, R.anim.translate_anim);
            //mainLayout.setAnimation(translate_anim);
        }
    }


}

