package com.example.myapplication.fragments.loginRegister

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.activities.MainActivityNoConnectionRecipes
import com.example.myapplication.databinding.FragmentLoginBinding
import com.example.myapplication.dialog.setupBottomSheetDialog
import com.example.myapplication.util.ConnectionLiveData
import com.example.myapplication.util.Resource
import com.example.myapplication.viewmodel.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var binding: FragmentLoginBinding
    private val viewModel by viewModels<LoginViewModel>()
    lateinit var connectionLiveData: ConnectionLiveData
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater)

        connectionLiveData = context?.let { ConnectionLiveData(it) }!!
        connectionLiveData.observe(viewLifecycleOwner) {networkAvailable->
            if(networkAvailable){

                binding.internetLayout.visibility = VISIBLE
                binding.noInternetLayout.visibility = GONE
            }
            else{

                binding.noInternetLayout.visibility = VISIBLE
                binding.internetLayout.visibility = GONE
            }

        }
        drawLayout()

        binding.tryAgainButton.setOnClickListener {
            drawLayout()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonerror.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_introductionFragment)
        }

        binding.apply {
            buttonsigin.setOnClickListener {
                val email = emailSignIn.text.toString().trim()
                val password = passwordsignin.text.toString()
                viewModel.login(email, password)
            }
        }

        binding.tvForgotPassword.setOnClickListener {
            setupBottomSheetDialog { email ->
                viewModel.resetPassword(email)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.resetPassword.collect {
                when (it) {
                    is Resource.Loading -> {
                    }
                    is Resource.Succes -> {
                        Snackbar.make(
                            requireView(),
                            "Reset link was sent to your email",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                    is Resource.Error -> {
                        Snackbar.make(requireView(), "Error: ${it.message}", Snackbar.LENGTH_LONG)
                            .show()
                    }
                    else -> Unit
                }
            }

            lifecycleScope.launchWhenStarted {
                viewModel.resetPassword.collect {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Succes -> {
                            Snackbar.make(
                                requireView(),
                                "Reset link was sent to your email",
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                        is Resource.Error -> {
                            Snackbar.make(
                                requireView(),
                                "Error: ${it.message}",
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                        else -> Unit
                    }
                }
            }
        }
            lifecycleScope.launchWhenStarted {
                viewModel.login.collect {
                    when (it) {
                        is Resource.Loading -> {
                            binding.buttonsigin.startAnimation()
                        }
                        is Resource.Succes -> {
                            binding.buttonsigin.revertAnimation()
                            //Pruebaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                            Intent(requireActivity(), MainActivityNoConnectionRecipes::class.java).also { intent ->
                                intent.addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            or Intent.FLAG_ACTIVITY_NEW_TASK
                                )
                                startActivity(intent)
                            }
                        }
                        is Resource.Error -> {
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                            binding.buttonsigin.revertAnimation()
                        }
                        else -> Unit
                    }
                }
            }


        }

    private fun isNetworkAvailable(): Boolean {
        val cm = getActivity()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }

    private fun drawLayout() {
        if (isNetworkAvailable()) {
            binding.internetLayout.visibility = VISIBLE
            binding.noInternetLayout.visibility = GONE
        } else {
            binding.noInternetLayout.visibility = VISIBLE
            binding.internetLayout.visibility = GONE
        }
    }
    }