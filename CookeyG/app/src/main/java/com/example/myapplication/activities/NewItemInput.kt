package com.example.myapplication.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.adapters.InventoryRepository
import com.example.myapplication.data.IngredientUser
import com.example.myapplication.data.UnitsEnum
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NewItemInput:AppCompatActivity(), AdapterView.OnItemSelectedListener{
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()
    lateinit var userUID:String
    lateinit var invRepo: InventoryRepository
    var hotProduct = IngredientUser(name = "",
        quantity=0,
        componenteCalorico = null,
        unit = null,
        fechaExpiracion = null,
        img=null,
        id=""
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val productName: String? = intent.getStringExtra("product_name")
        val productImgLink: String? = intent.getStringExtra("product_image")
        val productQuantity: Int? = intent.getIntExtra("product_quantity",0)
        val productUnit: String? = intent.getStringExtra("product_unit")
        val isHotProduct: Boolean? = intent.getBooleanExtra("is_hot_product",false)

        setContentView(R.layout.activity_add_item)
        userUID = application.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE).getString("UID", "").toString()
        invRepo=InventoryRepository(userUID)

        val actionBar = supportActionBar
        actionBar!!.title = "Add new item"
        val inputProductName: EditText =findViewById(R.id.input_product_name)
        val inputProductQty: EditText =findViewById(R.id.input_product_quantity)
        val unitSelector : Spinner = findViewById(R.id.input_units)

        ArrayAdapter.createFromResource(
            this,
            R.array.item_units,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // Apply the adapter to the spinner
            unitSelector.adapter = adapter
        }
        unitSelector.onItemSelectedListener=this@NewItemInput

        if(isHotProduct!==null && isHotProduct){
            db.collection("hotproduct")
                .get()
                .addOnSuccessListener { documents ->
                    Log.d("Success hot product size", documents.size().toString())
                    for (document in documents) {
                        Log.d("hot product", "${document.id} => ${document.data}")
                        hotProduct.name= document.data["name"] as String
                        hotProduct.img= document.data["image"] as String
                        hotProduct.quantity= document.data["quantity"] as Long
                        if(document.data["unit"] === "g"){
                            hotProduct.unit= UnitsEnum.GRAM
                        }
                    }
                    inputProductName.setText(hotProduct.name)
                    inputProductQty.setText(hotProduct.quantity.toString())
                }
                .addOnFailureListener { exception ->
                    Log.w("TAG", "Error getting documents: ", exception)
                }
        }else if(productName!=null){
                inputProductName.setText(productName)
        }

        val addItemBtn: Button = findViewById(R.id.add_new_item_button)
        addItemBtn.setOnClickListener {
            //val userUID = application.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE).getString("UID", "")
            hotProduct.name=inputProductName.text.toString()
            hotProduct.quantity=inputProductQty.text.toString().toLong()

            GlobalScope.launch(Dispatchers.IO){
                if (userUID != null) {
                    invRepo.addNewItem(hotProduct, userUID)
                }
            }
            finish()
        }
        
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null) {
            Log.d("hot product unit", parent.getItemAtPosition(position) as String + " "+position)
            (parent.getChildAt(0) as TextView).setTextColor(Color.argb(100, 40,89, 67))
            when(position){
                0->hotProduct.unit=UnitsEnum.GRAM
                1->hotProduct.unit=UnitsEnum.LITER
                2->hotProduct.unit=UnitsEnum.UNIT
            }
        }
    }
}