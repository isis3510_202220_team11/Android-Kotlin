package com.example.myapplication.adapters
import android.media.metrics.Event
import android.util.Log
import com.example.myapplication.data.IngredientUser
import com.example.myapplication.data.Promos
import com.example.myapplication.data.UnitsEnum
import com.google.firebase.firestore.*
import java.time.DateTimeException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date

class InventoryRepository(var userUID:String) {
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var search: SearchBarcode= SearchBarcode()

    suspend fun getAllItems(userUID:String, inventoryArrayList:ArrayList<IngredientUser>, inventoryAdapter: InventoryAdapter) {
        db = FirebaseFirestore.getInstance()
        db.collection("user")
            .document(userUID!!)
            .collection("inventory")
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Log.e("Firestore Error", error.message.toString())
                } else {
                    if (value != null) {
                        for (document in value.documents) {
                            val ingredient = IngredientUser("", null, null, 0, null, null, "")
                            ingredient.name = document.data!!["name"].toString()
                            ingredient.quantity = document.data!!["quantity"].toString().toLong()
                            ingredient.img = document.data!!["image"].toString()
                            when (document.data!!["unit"]) {
                                "GRAM" -> ingredient.unit = UnitsEnum.GRAM
                                "g" -> ingredient.unit = UnitsEnum.GRAM
                                "UNIT" -> ingredient.unit = UnitsEnum.UNIT
                                "units" -> ingredient.unit = UnitsEnum.UNIT
                                "LITER" -> ingredient.unit = UnitsEnum.LITER
                                "l" -> ingredient.unit = UnitsEnum.LITER
                                else -> ingredient.unit = UnitsEnum.GRAM
                            }
                            ingredient.id = document.id
                            if(ingredient in inventoryArrayList){
                                Log.e("Firestore Error", "Already in the inventory")
                            }
                            else{
                                inventoryArrayList.add(ingredient)
                                inventoryAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }
            }
    }
    suspend fun addNewItem(item:IngredientUser, userUID:String) {
        var stringUnit=""
            when(item.unit){
                UnitsEnum.GRAM->stringUnit="g"
                UnitsEnum.LITER->stringUnit="l"
                UnitsEnum.UNIT->stringUnit="u"
            }
        var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss")
        val data = hashMapOf(
            "name" to item.name,
            "quantity" to item.quantity,
            "unit" to item.unit.toString(),
            "image" to (search.findInfoFromBarcode("image_of_a_"+item.name.replace(" ","_"))?.imageLink ?: ""),
            "date" to LocalDateTime.now().format(formatter).toString()
        )
        db.collection("user")
            .document(userUID!!)
            .collection("inventory")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d("Item created", "DocumentSnapshot written with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("Item not created", "Error adding document", e)
            }
    }

    suspend fun deleteItem(item:IngredientUser, inventoryAdapter: InventoryAdapter) {

        db.collection("user")
            .document(userUID)
            .collection("inventory")
            .document(item.id)
            .delete()
        .addOnSuccessListener {   Log.d("Delete success", "DocumentSnapshot successfully deleted!")
            inventoryAdapter.itemList.remove(item)
            inventoryAdapter.notifyDataSetChanged()
        }
        .addOnFailureListener { e -> Log.w("Delete error","Error deleting document", e) }
    }

    companion object {
        private const val DATABASE_NAME = "Inventory.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "local_inventory"
        private const val USER_UID = "user_uid"
        private const val COLUMN_NAME = "product_name"
        private const val COLUMN_QUANTITY = "product_quantity"
        private const val COLUMN_UNIT = "product_unit"
        private const val COLUMN_IMAGE = "product_image"
    }
}