package com.example.myapplication.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.myapplication.R
import com.example.myapplication.adapters.SearchBarcode
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import java.io.File

class ShowBarcodeSearchResult : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode_info)

        val barcodeImg:Bitmap? = intent.getParcelableExtra<Bitmap>("barcodeImg")
        val scanBtn : TextView = findViewById(R.id.barcodeNumber)
        val takenPhoto : ImageView = findViewById(R.id.APIImage)
        takenPhoto.setImageBitmap(barcodeImg)
        takenPhoto.scaleX=2.0F
        takenPhoto.scaleY=2.0F
        if(barcodeImg!==null) {
            val file = File(this.filesDir, "temp.jpg")
            lifecycleScope.launch{
                val result = SearchBarcode().searchBarcodeByPhoto(file)
                if(result === null){
                    scanBtn.text = "There was no barcode detected"
                }else{
                    scanBtn.text = result.title
                    if(result.title!="Product not found"){
                        Picasso.with(this@ShowBarcodeSearchResult).load(result.imageLink).into(takenPhoto)
                        val intent = Intent(this@ShowBarcodeSearchResult, NewItemInput::class.java)
                        intent.putExtra("product_name",result.title)
                        intent.putExtra("product_image",result.imageLink)
                        startActivity(intent)
                    }
                }
            }
        }else{
            scanBtn.text = "There was an error while reading your barcode"
        }
    }
}