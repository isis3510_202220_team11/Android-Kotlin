package com.example.myapplication.adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.UserRecipe
import com.example.myapplication.util.MyCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class MyAdapter3(private val userList: ArrayList<UserRecipe>) : RecyclerView.Adapter<MyAdapter3.MyViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int){

        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter3.MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item3,
            parent, false)

        return MyViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: MyAdapter3.MyViewHolder, position: Int) {


        val userRecipe : UserRecipe = userList[position]
        holder.name.text = userRecipe.name
        GlobalScope.launch (Dispatchers.IO){
            getImage(userRecipe.img, holder)
        }
        holder.calories.text = userRecipe.calories.toString()
        //holder.recipe.text = userRecipe.recipe
        holder.likedBy.text = userRecipe.likedBy.toString()
        holder.type.text = userRecipe.type.toString()
        holder.country.text =userRecipe.country.toString()


    }

    private suspend fun getImage(imgURL: String ?= null, holder: MyViewHolder) {
        var img1 = MyCache.instance.retrieveBitmapFromCache(imgURL!!)
        if (img1 != null) {
            withContext(Dispatchers.Main) {
                holder.img.setImageBitmap(img1)
            }
        } else {
            try {
                val url = URL(imgURL)
                img1 = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                MyCache.instance.saveBitmapToCahche(imgURL, img1)
                withContext(Dispatchers.Main) {
                    holder.img.setImageBitmap(img1)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    holder.img.setImageResource(R.drawable.noavailable)
                }
            }

            //val url = URL(imgURL)
            //val img = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            //withContext(Dispatchers.Main){
            //  holder.img.setImageBitmap(img)
            //}

        }
    }

    override fun getItemCount(): Int {

        return userList.size

    }

    public class MyViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){

        val name : TextView = itemView.findViewById(R.id.recipeName2)
        val img : ImageView = itemView.findViewById(R.id.recipesImage2)
        val calories : TextView = itemView.findViewById(R.id.recipeCalories2)
        //val recipe : TextView = itemView.findViewById(R.id.recipeRecipe)
        val likedBy : TextView = itemView.findViewById(R.id.recipeLikedBy2)
        val type : TextView = itemView.findViewById(R.id.recipeType2)
        val country: TextView = itemView.findViewById(R.id.country3)

        init {
            itemView.setOnClickListener {

                listener.onItemClick(adapterPosition)

            }
        }
    }
}