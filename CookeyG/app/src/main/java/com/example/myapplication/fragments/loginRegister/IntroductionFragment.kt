package com.example.myapplication.fragments.loginRegister

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentFirstPageBinding
import com.example.myapplication.util.ConnectionLiveData

class IntroductionFragment: Fragment(R.layout.fragment_first_page) {
    private lateinit var binding: FragmentFirstPageBinding
    lateinit var connectionLiveData: ConnectionLiveData
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFirstPageBinding.inflate(inflater)

        connectionLiveData = context?.let { ConnectionLiveData(it) }!!
        connectionLiveData.observe(viewLifecycleOwner) {networkAvailable->
            if(networkAvailable){

                binding.internetLayout.visibility = VISIBLE
                binding.noInternetLayout.visibility = GONE
            }
            else{

                binding.noInternetLayout.visibility = VISIBLE
                binding.internetLayout.visibility = GONE
            }

        }

        drawLayout()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drawLayout()
        binding.button.setOnClickListener{
            findNavController().navigate(R.id.action_introductionFragment_to_loginFragment)

        }
        binding.button2.setOnClickListener{
            findNavController().navigate(R.id.action_introductionFragment_to_registerFragment)

        }
    }


    private fun isNetworkAvailable(): Boolean {
        val cm = getActivity()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }

    private fun drawLayout() {
        if (isNetworkAvailable()) {
            binding.internetLayout.visibility = VISIBLE
            binding.noInternetLayout.visibility = GONE
        } else {
            binding.noInternetLayout.visibility = VISIBLE
            binding.internetLayout.visibility = GONE
        }
    }
}