package com.example.myapplication.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.InventoryAdapter
import com.example.myapplication.adapters.InventoryRepository
import com.example.myapplication.data.IngredientUser
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.firestore.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class InventoryActivity : AppCompatActivity() {
    lateinit var connectionLiveData: ConnectionLiveData
    private lateinit var db: FirebaseFirestore
    private lateinit var recyclerView : RecyclerView
    private lateinit var inventoryArrayList : ArrayList<IngredientUser>
    private lateinit var inventoryAdapter : InventoryAdapter
    private lateinit var inventoryRepository: InventoryRepository
    private lateinit var userUID : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_inventory)

        val actionBar = supportActionBar
        actionBar!!.title = "Inventory"

        val addItemBtn: Button = findViewById(R.id.scan_product_button)
        addItemBtn.setOnClickListener(){
            val intent = Intent(this, SelectInventoryInput::class.java)
            startActivity(intent)
        }

        userUID =
            application.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE).getString("UID", "")
                .toString()

        inventoryRepository= InventoryRepository(userUID)
        recyclerView = findViewById(R.id.items_inventario)
        recyclerView.layoutManager = LinearLayoutManager(this)

        inventoryArrayList = arrayListOf()
        inventoryAdapter = InventoryAdapter(inventoryArrayList, inventoryRepository)
        recyclerView.adapter = inventoryAdapter
    }

    override fun onResume() {
        super.onResume()

        //Checks the connection status and informs the user if it's offline
        val statusText: TextView = findViewById(R.id.syncStatusInventory)
        val addItemBtn: Button = findViewById(R.id.scan_product_button)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable) {
                addItemBtn.isEnabled = true
            }else{
                statusText.text = "No internet connection"
                addItemBtn.isEnabled = false
            }
        }

        lifecycleScope.launch(Dispatchers.IO){
            inventoryRepository.getAllItems(userUID, inventoryArrayList, inventoryAdapter)
        }
    }
}