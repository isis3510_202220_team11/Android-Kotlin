package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.PromosAdapter
import com.example.myapplication.data.Promos
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.firestore.*

class PromosActivity : AppCompatActivity()  {
    private lateinit var recyclerView : RecyclerView
    private lateinit var userArrayList : ArrayList<Promos>
    private lateinit var myAdapter : PromosAdapter
    private lateinit var db: FirebaseFirestore
    private lateinit var checkInternet: Button

    var connectivityManager: ConnectivityManager? = null
    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

            }
            else{
                val intent = Intent(this, LocalStoragePromos::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.activity_promos)

        val actionBar = supportActionBar
        actionBar!!.title = "Promos"

        actionBar.setDisplayHomeAsUpEnabled(true)


        recyclerView = findViewById(R.id.vistaspromos)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        userArrayList = arrayListOf()

        //------------------------------

        var adapter = PromosAdapter(userArrayList)
        myAdapter = adapter
        adapter.setOnItemClickListener(object : PromosAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                val intent = Intent(this@PromosActivity, SpecificInfoPromosActivity::class.java)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("img", userArrayList[position].img)
                intent.putExtra("before", userArrayList[position].before)
                intent.putExtra("after", userArrayList[position].after)
                intent.putExtra("place", userArrayList[position].place)
                intent.putExtra("discount", userArrayList[position].discount.toString())
                intent.putExtra("description", userArrayList[position].description)
                startActivity(intent)

            }
        })

        recyclerView.adapter = myAdapter

        eventChangeListener()

        checkInternet = findViewById(R.id.marketadvisepromos)
        checkInternet.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()

            if (isConnectedTrue) {
                val i = Intent(this@PromosActivity, BestDealsActivity::class.java)
                startActivity(i)
            } else {
                val i = Intent(this@PromosActivity, BestDealsActivity::class.java)
                startActivity(i)

            }
        })

    }

    //Correcto siuuu
    private fun eventChangeListener() {

        db = FirebaseFirestore.getInstance()
        //db.collection("Recipes").orderBy("name", Query.Direction.ASCENDING)
        db.collection("promos").orderBy("name", Query.Direction.ASCENDING)
            .
            addSnapshotListener(object : EventListener<QuerySnapshot>
            {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {

                    if (error != null){

                        Log.e("Firestore Error", error.message.toString())
                        return

                    }

                    for(dc : DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            userArrayList.add(dc.document.toObject(Promos::class.java))
                        }
                    }

                    myAdapter.notifyDataSetChanged()

                }

            })


    }
    var isConnectedTrue = false
    var myCallBack: ConnectivityManager.NetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            isConnectedTrue = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            isConnectedTrue = false
        }
    }

    private fun registerCallBack() {
        connectivityManager =
            applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager!!.registerDefaultNetworkCallback(myCallBack)
        }
    }

    private fun unRegisterCallBack() {
        if (connectivityManager == null) return
        connectivityManager!!.unregisterNetworkCallback(myCallBack)
    }

    override fun onResume() {
        super.onResume()
        registerCallBack()
    }

    override fun onPause() {
        super.onPause()
        unRegisterCallBack()
    }
}