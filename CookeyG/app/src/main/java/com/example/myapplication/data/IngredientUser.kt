package com.example.myapplication.data

import java.sql.Date

data class IngredientUser(
    var name : String,
    val componenteCalorico : Long?,
    var unit : UnitsEnum?,
    var quantity : Long,
    val fechaExpiracion : Date?,
    var img : String?,
    var id:String
    )