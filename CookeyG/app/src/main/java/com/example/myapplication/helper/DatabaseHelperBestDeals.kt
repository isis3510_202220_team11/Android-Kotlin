package com.example.myapplication.helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

class DatabaseHelperBestDeals(private val context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = "CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_BEFORE + " TEXT, " +
                COLUMN_AFTER + " TEXT, " +
                COLUMN_IMG + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_PLACE + " TEXT, " +
                COLUMN_DISCOUNT + " TEXT);"
        db.execSQL(query)
        //addRecipe();
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun addPromo() {
        deleteAllData()
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "ACAI POWDER ")
        cv.put(COLUMN_BEFORE, "4.18 USD")
        cv.put(COLUMN_AFTER, "10.37 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/Acaii.png?alt=media&token=f6775480-110e-4a1f-ab26-4d02590262cd"
        )
        cv.put(COLUMN_DESCRIPTION, "ACAI FREEZE DRIED POWDER SUPERFUDS 80 gr")
        cv.put(COLUMN_PLACE, "Alkosto")
        cv.put(COLUMN_DISCOUNT, "50")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo2()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo2() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Canned tuna")
        cv.put(COLUMN_BEFORE, "0.99 USD")
        cv.put(COLUMN_AFTER, "1.82 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/atun.png?alt=media&token=d3811dff-01df-4a3c-a59f-8b5c5ef58d3e"
        )
        cv.put(COLUMN_DESCRIPTION, "Frescampo. Preserved In Tomato Sauce 425")
        cv.put(COLUMN_PLACE, "Exito")
        cv.put(COLUMN_DISCOUNT, "46")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo3()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo3() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Organic Agave Syrup")
        cv.put(COLUMN_BEFORE, "6.08 USD")
        cv.put(COLUMN_AFTER, "2.61 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/sirope.png?alt=media&token=94a9dc38-7777-4c5b-82fd-1a2fad4635ea"
        )
        cv.put(COLUMN_DESCRIPTION, "TAEQ Organic Agave Syrup 330 gr. Type of Product: Natural Food")
        cv.put(COLUMN_PLACE, "D1")
        cv.put(COLUMN_DISCOUNT, "44")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo4()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo4() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "PIETRAN ham")
        cv.put(COLUMN_BEFORE, "1.97 USD")
        cv.put(COLUMN_AFTER, "0.79 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/jamon.jpg?alt=media&token=5f1f46ce-5970-4697-bf1b-0d0160cc3a72"
        )
        cv.put(COLUMN_DESCRIPTION, "PIETRÁN Standard Fat-Free Ham Reduced Sodium x230g. Product Presentation: Package")
        cv.put(COLUMN_PLACE, "Alkosto")
        cv.put(COLUMN_DISCOUNT, "40")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo5()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo5() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "TAEQ CHIA SEED")
        cv.put(COLUMN_BEFORE, "3.38 USD")
        cv.put(COLUMN_AFTER, "1.36 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/semillasdeChia.png?alt=media&token=da0e902c-62c4-4ab9-8ab6-4bb022d73608"
        )
        cv.put(COLUMN_DESCRIPTION, "CHIA SEED TAEQ 225 gr. Product Type: Wheat and Sesame")
        cv.put(COLUMN_PLACE, "Alkosto")
        cv.put(COLUMN_DISCOUNT, "40")
        val result = db.insert(TABLE_NAME, null, cv)
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun readAllData(): Cursor? {
        val query = "SELECT * FROM " + TABLE_NAME
        val db = this.readableDatabase
        var cursor: Cursor? = null
        if (db != null) {
            cursor = db.rawQuery(query, null)
        }
        return cursor
    }


    fun deleteAllData() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM " + TABLE_NAME)
    } //DeleteVideo5Minuto9

    companion object {
        private const val DATABASE_NAME = "Promos.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "my_promos"
        private const val COLUMN_BEFORE = "promo_before"
        private const val COLUMN_AFTER = "promo_after"
        private const val COLUMN_IMG = "promo_img"
        private const val COLUMN_DESCRIPTION = "promo_description"
        private const val COLUMN_NAME = "promo_name"
        private const val COLUMN_PLACE = "promo_place"
        private const val COLUMN_DISCOUNT = "promo_discount"
    }
}