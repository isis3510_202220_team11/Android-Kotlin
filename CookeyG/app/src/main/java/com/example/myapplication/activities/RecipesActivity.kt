package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.MyAdapter
import com.example.myapplication.data.UserRecipe
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.*
import com.google.firebase.ktx.Firebase


class RecipesActivity : AppCompatActivity()  {

    private lateinit var recyclerView : RecyclerView
    private lateinit var userArrayList : ArrayList<UserRecipe>
    private lateinit var myAdapter : MyAdapter
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var nombre: String

    private lateinit var checkInternet: Button
    var connectivityManager: ConnectivityManager? = null
    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

            }
            else{
                val intent = Intent(this, LocalStorageRecipes::class.java)
                startActivity(intent)
            }

        }

        setContentView(R.layout.activity_recipes)
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        val actionBar = supportActionBar
        actionBar!!.title = "Recipes"

        actionBar.setDisplayHomeAsUpEnabled(true)


        checkInternet = findViewById(R.id.buttonFor)
        checkInternet.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()

            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Top5")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle)

            if (isConnectedTrue) {
                val i = Intent(this@RecipesActivity, TopActivity::class.java)
                startActivity(i)
            } else {
                val i = Intent(this@RecipesActivity, NoInternetTopActivity::class.java)
                startActivity(i)
            }
        })



        recyclerView = findViewById(R.id.vistasAs)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        userArrayList = arrayListOf()

        //------------------------------

        var adapter = MyAdapter(userArrayList)
        myAdapter = adapter
        adapter.setOnItemClickListener(object : MyAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                val intent = Intent(this@RecipesActivity, SpecificInfoMenuActivity::class.java)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("calories", userArrayList[position].calories.toString())
                intent.putExtra("country", userArrayList[position].country)
                intent.putExtra("likedBy", userArrayList[position].likedBy.toString())
                intent.putExtra("recipe", userArrayList[position].recipe)
                intent.putExtra("img", userArrayList[position].img)
                intent.putExtra("type", userArrayList[position].type   )
                startActivity(intent)

            }
        })

        recyclerView.adapter = myAdapter

       EventChangeListener()

    }

    private fun EventChangeListener() {

        db = FirebaseFirestore.getInstance()
        db.collection("Recipes").orderBy("name", Query.Direction.ASCENDING)
            //db.collection("Recipes").orderBy("name", Query.Direction.ASCENDING).limit(5)
            .
        addSnapshotListener(object : EventListener<QuerySnapshot>
        {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {

                if (error != null){

                    Log.e("Firestore Error", error.message.toString())
                    return

                }

                for(dc : DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        userArrayList.add(dc.document.toObject(UserRecipe::class.java))
                    }
                }

                myAdapter.notifyDataSetChanged()

            }

        })


    }

    var isConnectedTrue = false
    var myCallBack: ConnectivityManager.NetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            isConnectedTrue = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            isConnectedTrue = false
        }
    }

    //-----------------------------------------------------------
    private fun registerCallBack() {
        connectivityManager =
            applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager!!.registerDefaultNetworkCallback(myCallBack)
        }
    }

    private fun unRegisterCallBack() {
        if (connectivityManager == null) return
        connectivityManager!!.unregisterNetworkCallback(myCallBack)
    }

    override fun onResume() {
        super.onResume()
        registerCallBack()
    }

    override fun onPause() {
        super.onPause()
        unRegisterCallBack()
    }

}