package com.example.myapplication.util

import android.graphics.Bitmap
import androidx.collection.LruCache

class CachePromos private constructor() {

    private object HOLDER {
        val INSTANCE = CachePromos()
    }

    companion object {
        val instance: CachePromos by lazy { HOLDER.INSTANCE }
    }
    val lru: LruCache<Any, Any>

    final var maxMemory : Int = (Runtime.getRuntime().maxMemory()/1024).toInt()

    init {

        lru = LruCache(maxMemory/8)

    }

    fun saveBitmapToCahche(key: String, bitmap: Bitmap) {

        try {
            MyCache.instance.lru.put(key, bitmap)
        } catch (e: Exception) {
        }

    }

    fun retrieveBitmapFromCache(key: String): Bitmap? {

        try {
            return MyCache.instance.lru.get(key) as Bitmap?
        } catch (e: Exception) {
        }

        return null
    }

}