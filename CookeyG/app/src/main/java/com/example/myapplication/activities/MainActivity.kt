package com.example.myapplication.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val networkConnection = NetworkConnection(applicationContext)
        //networkConnection.observe(this, Observer { isConnected ->
          //  if(isConnected){
                //Vista con Internet - NetworkConnection at util
            //}
            //else{
                //Vista sin Internet
            //}
        //})

        val actionBar = supportActionBar
        actionBar!!.title = "Menu"

        //
        val pruebaUnoo: Button = findViewById(R.id.button2)


        pruebaUnoo.setOnClickListener(){
            val intent = Intent(this, LocalStorageRecipes::class.java)
            startActivity(intent)
        }
        //

        //val boton: Button = findViewById(R.id.button2)


        //boton.setOnClickListener(){
          //  val intent = Intent(this, RecipesActivity::class.java)
            //startActivity(intent)
        //}

        val boton2: Button = findViewById(R.id.recommendations)


        boton2.setOnClickListener(){
            val intent = Intent(this, RecommendationsActivity::class.java)
            startActivity(intent)
        }

        val boton3: Button = findViewById(R.id.inventorybutton)


        boton3.setOnClickListener(){
            val intent = Intent(this, InventoryActivity::class.java)
            startActivity(intent)
        }

        boton2.setOnClickListener(){
            val intent = Intent(this, RecommendationsActivity::class.java)
            startActivity(intent)
        }

        val pruebaDos: Button = findViewById(R.id.button3)


        pruebaDos.setOnClickListener(){
            val intent = Intent(this, LocalStoragePromos::class.java)
            startActivity(intent)
        }

        val calories: Button = findViewById(R.id.recipesCaloriesR)


        calories.setOnClickListener(){
            val intent = Intent(this, LocalStorageCalories::class.java)
            startActivity(intent)
        }

        //val boton4: Button = findViewById(R.id.button3)



       // boton4.setOnClickListener(){
         //   val intent = Intent(this, PromosActivity::class.java)
          //  startActivity(intent)
        //}

    }
}