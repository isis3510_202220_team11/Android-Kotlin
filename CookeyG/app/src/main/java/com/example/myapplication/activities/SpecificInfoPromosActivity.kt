package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.util.ConnectionLiveData
import com.squareup.picasso.Picasso

class SpecificInfoPromosActivity : AppCompatActivity()  {
    private lateinit var name: TextView
    private lateinit var img: ImageView
    private lateinit var before: TextView
    private lateinit var after: TextView
    private lateinit var place: TextView
    private lateinit var discount: TextView
    private lateinit var description: TextView
    lateinit var namei: String
    private lateinit var checkInternet: Button
    var connectivityManager: ConnectivityManager? = null
    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

            }
            else{
                val i = Intent(this, LocalStorageSpecificPromos::class.java)
                i.putExtra("name", namei)
                startActivity(i)
            }

        }

        setContentView(R.layout.activity_specific_promos)

        val actionBar = supportActionBar
        actionBar!!.title = "Promo Description"

        actionBar.setDisplayHomeAsUpEnabled(true)

        //initView()
        setValuesToViews()

    }

    private fun setValuesToViews(){

        name = findViewById(R.id.product_names)
        name.text = intent.getStringExtra("name")
        namei=name.toString()
        val url: String? = intent.getStringExtra("img")
        img = findViewById(R.id.img_products)
        Picasso.with(this).load(url).into(img)
        before = findViewById(R.id.beforetexts)
        before.text = intent.getStringExtra("before")
        after = findViewById(R.id.aftertexts)
        after.text = intent.getStringExtra("after")
        place = findViewById(R.id.places)
        place.text = intent.getStringExtra("place")
        discount = findViewById(R.id.discounts)
        discount.text = intent.getStringExtra("discount")
        description = findViewById(R.id.descriptions)
        description.text = intent.getStringExtra("description")



    }
}