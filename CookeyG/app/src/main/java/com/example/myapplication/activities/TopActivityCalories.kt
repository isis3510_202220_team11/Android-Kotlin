package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.TopCaloriesAdapter
import com.example.myapplication.data.UserRecipe2
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.firestore.*

class TopActivityCalories : AppCompatActivity()  {
    private lateinit var recyclerView : RecyclerView
    private lateinit var userArrayList : ArrayList<UserRecipe2>
    private lateinit var myAdapter : TopCaloriesAdapter
    private lateinit var db: FirebaseFirestore
    private var caloriesAmount: Int = 0

    private lateinit var internetLayout: RelativeLayout
    private lateinit var noInternetLayout: RelativeLayout

    lateinit var connectionLiveData: ConnectionLiveData
    private lateinit var tryAgainButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        caloriesAmount = intent.getIntExtra("key", 0)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){
            }
            else{
                //Cambiar esto
                val intent = Intent(this, NoInternetCaloriesAmount::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.activity_calories_recommendations)

        val actionBar = supportActionBar
        actionBar!!.title = "Recipes by amount of calories"


        actionBar.setDisplayHomeAsUpEnabled(true)


        recyclerView = findViewById(R.id.vistasAs2Locationcalories)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        userArrayList = arrayListOf()

        var adapter = TopCaloriesAdapter(userArrayList)
        myAdapter = adapter
        adapter.setOnItemClickListener(object : TopCaloriesAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                val intent = Intent(this@TopActivityCalories, SpecificInfoMenuActivity::class.java)
                intent.putExtra("name", userArrayList[position].name)
                intent.putExtra("calories", userArrayList[position].calories.toString())
                intent.putExtra("country", userArrayList[position].country)
                intent.putExtra("likedBy", userArrayList[position].likedBy.toString())
                intent.putExtra("recipe", userArrayList[position].recipe)
                intent.putExtra("img", userArrayList[position].img)
                intent.putExtra("type", userArrayList[position].type   )
                startActivity(intent)

            }
        })

        recyclerView.adapter = myAdapter

        EventChangeListener()

    }

    private fun EventChangeListener() {

        db = FirebaseFirestore.getInstance()
        //db.collection("Recipes").orderBy("name", Query.Direction.ASCENDING)
            db.collection("Recipes").whereLessThanOrEqualTo("calories",caloriesAmount).orderBy("calories", Query.Direction.ASCENDING)
            .
            addSnapshotListener(object : EventListener<QuerySnapshot>
            {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {

                    if (error != null){

                        Log.e("Firestore Error", error.message.toString())
                        return

                    }

                    for(dc : DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            userArrayList.add(dc.document.toObject(UserRecipe2::class.java))
                        }
                    }

                    myAdapter.notifyDataSetChanged()

                }

            })


    }

    private fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET))

    }

    private fun drawLayout() {
        if (isNetworkAvailable()) {
            internetLayout.visibility = VISIBLE
            noInternetLayout.visibility = GONE
        } else {
            noInternetLayout.visibility = VISIBLE
            internetLayout.visibility = GONE
        }
    }
}