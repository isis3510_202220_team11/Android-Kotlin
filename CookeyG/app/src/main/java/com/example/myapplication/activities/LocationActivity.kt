package com.example.myapplication.activities


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.R
import com.example.myapplication.util.ConnectionLiveData
import java.util.*


class LocationActivity : AppCompatActivity(), LocationListener {

    private lateinit var checkInternet: Button
    lateinit var connectionLiveData: ConnectionLiveData
    var connectivityManager: ConnectivityManager? = null
    lateinit var pais: String


    var locationManager: LocationManager? = null
    var tvCity: TextView? = null
    var tvState: TextView? = null
    var tvCountry: TextView? = null
    var tvPin: TextView? = null
    var tvLocality: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

            }
            else{
                val intent = Intent(this, NoInternetLocationRecommendations::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.location)
        val actionBar = supportActionBar
        actionBar!!.title = "Location"

        actionBar.setDisplayHomeAsUpEnabled(true)
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 101
            )
        }
        tvCity = findViewById(R.id.tvCity)
        tvState = findViewById(R.id.tvState)
        tvCountry = findViewById(R.id.tvCountry)
        tvPin = findViewById(R.id.tvPin)
        tvLocality = findViewById(R.id.tvLocality)
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        locationEnabled()
        location


        checkInternet = findViewById(R.id.buttonLocation)
        checkInternet.setOnClickListener ({
            if (isConnectedTrue) {
                val i = Intent(this@LocationActivity, TopActivity2::class.java)
                i.putExtra("key", pais)
                startActivity(i)
                //finish
            } else {
                val i = Intent(this@LocationActivity, NoInternetLocationRecommendations::class.java)
                startActivity(i)
            }
        }

        )



    }

    private fun locationEnabled() {
        val lm = getSystemService(LOCATION_SERVICE) as LocationManager
        var gps_enabled = false
        var network_enabled = false
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder(this@LocationActivity)
                .setTitle("Enable GPS Service")
                .setMessage("We need your GPS location to show Near Places around you.")
                .setCancelable(false)
                .setPositiveButton(
                    "Enable"
                ) { paramDialogInterface, paramInt -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
                .setNegativeButton("Cancel", null)
                .show()
        }
    }

    val location: Unit
        get() {
            try {
                locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
                locationManager!!.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 500, 5f,
                    (this as LocationListener)
                )
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

    override fun onLocationChanged(location: Location) {
        try {
            val geocoder = Geocoder(applicationContext, Locale.getDefault())
            val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            tvCity!!.text = addresses[0].locality
            tvState!!.text = addresses[0].adminArea
            tvCountry!!.text = addresses[0].countryName
            pais = addresses[0].countryName.toString()
            tvPin!!.text = addresses[0].postalCode
            tvLocality!!.text = addresses[0].getAddressLine(0)
        } catch (e: Exception) {
        }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == 101) {
            location
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onProviderDisabled(provider: String) {}

    var isConnectedTrue = false
    var myCallBack: ConnectivityManager.NetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            isConnectedTrue = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            isConnectedTrue = false
        }
    }

    //-----------------------------------------------------------
    private fun registerCallBack() {
        connectivityManager =
            applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager!!.registerDefaultNetworkCallback(myCallBack)
        }
    }

    private fun unRegisterCallBack() {
        if (connectivityManager == null) return
        connectivityManager!!.unregisterNetworkCallback(myCallBack)
    }

    override fun onResume() {
        super.onResume()
        registerCallBack()
    }

    override fun onPause() {
        super.onPause()
        unRegisterCallBack()
    }
}
