package com.example.myapplication.activities


import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.util.ConnectionLiveData
import com.google.android.material.textfield.TextInputEditText


class CaloriesMainActivity : AppCompatActivity() {

    private lateinit var checkInternet: Button
    lateinit var connectionLiveData: ConnectionLiveData
    var caloriesAmount: Int = 0
    var connectivityManager: ConnectivityManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

            }
            else{
                val intent = Intent(this, LocalStorageCalories::class.java)
                startActivity(intent)
            }

        }
        setContentView(R.layout.calories_activity)
        val actionBar = supportActionBar
        actionBar!!.title = "Recipes by amount of calories"

        actionBar.setDisplayHomeAsUpEnabled(true)


        checkInternet = findViewById(R.id.buttonAmountCalories)
        checkInternet.setOnClickListener ({
            if (isConnectedTrue) {
                val valor = findViewById<TextInputEditText>(R.id.textInputCalories)
                caloriesAmount = valor.text.toString().toInt()
                val i = Intent(this@CaloriesMainActivity, TopActivityCalories::class.java)
                i.putExtra("key", caloriesAmount)
                startActivity(i)
                //finish
            } else {
                val i = Intent(this@CaloriesMainActivity, NoInternetCaloriesAmount::class.java)
                startActivity(i)
            }
        }

        )



    }

    var isConnectedTrue = false
    var myCallBack: ConnectivityManager.NetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            isConnectedTrue = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            isConnectedTrue = false
        }
    }

    //-----------------------------------------------------------
    private fun registerCallBack() {
        connectivityManager =
            applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager!!.registerDefaultNetworkCallback(myCallBack)
        }
    }

    private fun unRegisterCallBack() {
        if (connectivityManager == null) return
        connectivityManager!!.unregisterNetworkCallback(myCallBack)
    }

    override fun onResume() {
        super.onResume()
        registerCallBack()
    }

    override fun onPause() {
        super.onPause()
        unRegisterCallBack()
    }
}
