package com.example.myapplication.helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast


class DatabaseHelperCalories(private val context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = "CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_CALORIES + " TEXT, " +
                COLUMN_COUNTRY + " TEXT, " +
                COLUMN_IMG + " TEXT, " +
                COLUMN_RECIPE + " TEXT, " +
                COLUMN_TYPE + " TEXT, " +
                COLUMN_LIKEDBY + " TEXT);"
        db.execSQL(query)
        //addRecipe();
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun addRecipe() {
        deleteAllData()
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Brown Stew Chicken")
        cv.put(COLUMN_CALORIES, "334")
        cv.put(COLUMN_COUNTRY, "Colombia")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/chicken.png?alt=media&token=82962563-f07e-446c-9ba1-9d04b2c58b04"
        )
        cv.put(
            COLUMN_RECIPE,
            "Squeeze lime over chicken and rub well. Drain off excess lime juice. Combine tomato, scallion, onion, garlic, pepper, thyme, pimento and soy sauce in a large bowl with the chicken pieces. Cover and marinate at least one hour. Heat oil in a dutch pot or large saucepan. Shake off the seasonings as you remove each piece of chicken from the marinade. Reserve the marinade for sauce. Lightly brown the chicken a few pieces at a time in very hot oil. Place browned chicken pieces on a plate to rest while you brown the remaining pieces. Drain off excess oil and return the chicken to the pan. Pour the marinade over the chicken and add the carrots. Stir and cook over medium heat for 10 minutes. Mix flour and coconut milk and add to stew, stirring constantly. Turn heat down to minimum and cook another 20 minutes or until tender."
        )
        cv.put(COLUMN_TYPE, "Common")
        cv.put(COLUMN_LIKEDBY, "46")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe1()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe1() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Burek")
        cv.put(COLUMN_CALORIES, "240")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/burek.png?alt=media&token=cf552f77-50e5-4b71-ae8f-894f87165e66"
        )
        cv.put(
            COLUMN_RECIPE,
            "Fry the finely chopped onions and minced meat in oil. Add the salt and pepper. Grease a round baking tray and put a layer of pastry in it. Cover with a thin layer of filling and cover this with another layer of filo pastry which must be well coated in oil. Put another layer of filling and cover with pastry. When you have five or six layers, cover with filo pastry, bake at 200ºC/392ºF for half an hour and cut in quarters and serve."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "30")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe2()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe2() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Caesar Salad")
        cv.put(COLUMN_CALORIES, "350")
        cv.put(COLUMN_COUNTRY, "USA")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/caesarSalad.png?alt=media&token=3deac982-93f0-48a5-b32c-9a5c8b5f792a"
        )
        cv.put(
            COLUMN_RECIPE,
            "In a large mixing bowl, combine all of your ingredients and toss gently to coat the lettuce in caesar dressing. This recipe makes enough croutons for two full salads so you’ll have them ready to go for round 2!"
        )
        cv.put(COLUMN_TYPE, "Salads")
        cv.put(COLUMN_LIKEDBY, "15")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe3()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe3() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Fresh Sardines")
        cv.put(COLUMN_CALORIES, "400")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/sardines.png?alt=media&token=dd0bb52b-9db5-4366-a62b-91a795368ce6"
        )
        cv.put(
            COLUMN_RECIPE,
            "Wash the fish under the cold tap. Roll in the flour and deep fry in oil until crispy. Lay on kitchen towel to get rid of the excess oil and serve hot or cold with a slice of lemon."
        )
        cv.put(COLUMN_TYPE, "Assiatic")
        cv.put(COLUMN_LIKEDBY, "13")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe4()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe4() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Mushroom soup with buckwheat")
        cv.put(COLUMN_CALORIES, "255")
        cv.put(COLUMN_COUNTRY, "Colombia")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/mushroom.png?alt=media&token=cd07bbd3-8ba3-4bc4-8e18-a74cfca64fb8"
        )
        cv.put(
            COLUMN_RECIPE,
            "Chop the onion and garlic, slice the mushrooms and wash the buckwheat. Heat the oil and lightly sauté the onion. Add the mushrooms and the garlic and continue to sauté. Add the salt, vegetable seasoning, buckwheat and the bay leaf and cover with water. Simmer gently and just before it is completely cooked, add pepper, sour cream mixed with flour, the chopped parsley and vinegar to taste."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "237")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe5()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe5() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Shakshuka")
        cv.put(COLUMN_CALORIES, "345")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/shakshuka.png?alt=media&token=c83baadc-1c55-4f04-a0a9-4b02cf27ba02"
        )
        cv.put(
            COLUMN_RECIPE,
            "Heat the oil in a frying pan that has a lid, then soften the onions, chilli, garlic and coriander stalks for 5 mins until soft. Stir in the tomatoes and sugar, then bubble for 8-10 mins until thick. Can be frozen for 1 month. Using the back of a large spoon, make 4 dips in the sauce, then crack an egg into each one. Put a lid on the pan, then cook over a low heat for 6-8 mins, until the eggs are done to your liking. Scatter with the coriander leaves and serve with crusty bread."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "87")
        val result = db.insert(TABLE_NAME, null, cv)
        //addRecipe6()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    //Aqui se borró

    fun readAllData(): Cursor? {
        val query = "SELECT * FROM " + TABLE_NAME
        val db = this.readableDatabase
        var cursor: Cursor? = null
        if (db != null) {
            cursor = db.rawQuery(query, null)
        }
        return cursor
    }

    //void updateData(String row_id, String title, String author, String pages){
    //SQLiteDatabase db = this.getWritableDatabase();
    //ContentValues cv = new ContentValues();
    //cv.put(COLUMN_TITLE, title);
    //cv.put(COLUMN_AUTHOR, author);
    //cv.put(COLUMN_PAGES, pages);
    //long result = db.update(TABLE_NAME, cv, "_id=?", new String[]{row_id});
    //if(result == -1){
    //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
    //}else {
    //Toast.makeText(context, "Updated Successfully!", Toast.LENGTH_SHORT).show();
    //}
    //}
    //void deleteOneRow(String row_id){
    //SQLiteDatabase db = this.getWritableDatabase();
    //long result = db.delete(TABLE_NAME, "_id=?", new String[]{row_id});
    //if(result == -1){
    //Toast.makeText(context, "Failed to Delete.", Toast.LENGTH_SHORT).show();
    //}else{
    //Toast.makeText(context, "Successfully Deleted.", Toast.LENGTH_SHORT).show();

    fun deleteAllData() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM " + TABLE_NAME)
    } //DeleteVideo5Minuto9

    companion object {
        private const val DATABASE_NAME = "Recipes.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "my_recipes"
        private const val COLUMN_CALORIES = "recipe_calories"
        private const val COLUMN_COUNTRY = "recipe_country"
        private const val COLUMN_IMG = "recipe_img"
        private const val COLUMN_LIKEDBY = "recipe_likedBy"
        private const val COLUMN_NAME = "recipe_name"
        private const val COLUMN_RECIPE = "recipe_recipe"
        private const val COLUMN_TYPE = "recipe_type"
    }
}
