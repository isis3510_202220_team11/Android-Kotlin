package com.example.myapplication.activities

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso

class SpecificInfoMenuActivity2 : AppCompatActivity()   {




    private lateinit var name: TextView
    private lateinit var calories: TextView
    private lateinit var img: ImageView
    private lateinit var recipe: TextView
    private lateinit var type: TextView
    private lateinit var likedBy: TextView
    private lateinit var country: TextView


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_specific_info)

        val actionBar = supportActionBar
        actionBar!!.title = "Recipe Description"

        actionBar.setDisplayHomeAsUpEnabled(true)

        //initView()
        setValuesToViews()

        var buttonLike : Button = findViewById(R.id.botonLike)

        buttonLike.setOnClickListener(){
            openUpdateDialog()
            Toast.makeText(applicationContext,"Your like has been sent", Toast.LENGTH_SHORT).show()

        }


    }

    private fun openUpdateDialog(){
        var numero = Integer.parseInt(likedBy.text.toString())
        numero = numero +1
        val apodado = name.text.toString()
        var dbRef = FirebaseFirestore.getInstance()
        dbRef.collection("Recipes").document(apodado).update("likedBy", numero)
        likedBy = findViewById(R.id.recipeLikedBy3)
        likedBy.text = numero.toString()

    }

    private fun initView(){
    }

    private fun setValuesToViews(){

        name = findViewById(R.id.recipeName3)
        name.text = intent.getStringExtra("name")
        val url: String? = intent.getStringExtra("img")
        img = findViewById(R.id.recipesImage3)
        Picasso.with(this).load(url).into(img)
        calories = findViewById(R.id.recipeCalories3)
        calories.text = intent.getStringExtra("calories")
        recipe = findViewById(R.id.recipeRecipe3)
        recipe.text = intent.getStringExtra("recipe")
        type = findViewById(R.id.recipeType3)
        type.text = intent.getStringExtra("type")
        likedBy = findViewById(R.id.recipeLikedBy3)
        likedBy.text = intent.getStringExtra("likedBy")
        country = findViewById(R.id.country)
        country.text = intent.getStringExtra("country")



    }

}