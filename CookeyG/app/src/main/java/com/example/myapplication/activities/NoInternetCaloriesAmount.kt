package com.example.myapplication.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.util.ConnectionLiveData

class NoInternetCaloriesAmount : AppCompatActivity() {
    lateinit var connectionLiveData: ConnectionLiveData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){

                val intent = Intent(this, CaloriesMainActivity::class.java)
                startActivity(intent)
            }
            else{
            }

        }
        setContentView(R.layout.activity_calories_no_internet)
        val actionBar = supportActionBar
        actionBar!!.title = "Recipes by amount of calories"

        actionBar.setDisplayHomeAsUpEnabled(true)
    }
}