package com.example.myapplication.data

data class ProductResponse (
    var imageLink : String,
    var result : String,
    var title : String
        )