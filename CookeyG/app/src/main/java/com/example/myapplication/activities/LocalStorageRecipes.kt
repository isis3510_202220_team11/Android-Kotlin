package com.example.myapplication.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.helper.DatabaseHelper
import com.example.myapplication.util.ConnectionLiveData

class LocalStorageRecipes : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView

    lateinit var connectionLiveData: ConnectionLiveData
    //FloatingActionButton add_button;
    //ImageView empty_imageview;
    //TextView no_data;
    var pruebinha: CustomAdapterLocalStorage? = null
    var myDB: DatabaseHelper? = null
    var recipe_country: ArrayList<String>? = null
    var recipe_img: ArrayList<String>? = null
    var recipe_name: ArrayList<String>? = null
    var recipe_recipe: ArrayList<String>? = null
    var recipe_type: ArrayList<String>? = null
    var recipe_calories: ArrayList<String>? = null
    var recipe_likedBy: ArrayList<String>? = null
    //var customAdapter: CustomAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this) {networkAvailable->
            if(networkAvailable){
                val intent = Intent(this, RecipesActivity::class.java)
                startActivity(intent)
            }
            else{

            }

        }
        setContentView(R.layout.activity_recipes_no_connection)
        recyclerView = findViewById(R.id.vistasAs37)

        val actionBar = supportActionBar
        actionBar!!.title = "Recipes"

        actionBar.setDisplayHomeAsUpEnabled(true)

        //add_button = findViewById(R.id.add_button);
        //empty_imageview = findViewById(R.id.empty_imageview);
        //no_data = findViewById(R.id.no_data);
        //add_button.setOnClickListener(new View.OnClickListener() {
        //  @Override
        // public void onClick(View view) {
        //   Intent intent = new Intent(MainActivity.this, AddActivity.class);
        // startActivity(intent);
        //}
        //});
        myDB = DatabaseHelper(this@LocalStorageRecipes)
        recipe_country = ArrayList()
        recipe_img = ArrayList()
        recipe_likedBy = ArrayList()
        recipe_name = ArrayList()
        recipe_recipe = ArrayList()
        recipe_calories = ArrayList()
        recipe_likedBy = ArrayList()
        recipe_type = ArrayList()
        storeDataInArrays()
        pruebinha = CustomAdapterLocalStorage(
            this@LocalStorageRecipes,
            recipe_name!!,
            recipe_calories!!,
            recipe_country!!,
            recipe_img!!,
            recipe_recipe!!,
            recipe_type!!,
            recipe_likedBy!!
        )
        //customAdapter = new CustomAdapter(Ejemplo.this, recipe_name, recipe_calories, recipe_country, recipe_img, recipe_recipe, recipe_type, recipe_likedBy);
        //recyclerView.setAdapter(pruebinha)
        recyclerView.adapter = pruebinha
        recyclerView.layoutManager = LinearLayoutManager(this@LocalStorageRecipes)
    }

    //@Override
    //protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    //  super.onActivityResult(requestCode, resultCode, data);
    // if(requestCode == 1){
    //   recreate();
    //}
    //}
    fun storeDataInArrays() {
        myDB!!.addRecipe()
        val cursor = myDB!!.readAllData()
        if (cursor != null) {
            if (cursor.count == 0) {
                Toast.makeText(this, "No data.", Toast.LENGTH_SHORT).show()
                //empty_imageview.setVisibility(View.VISIBLE);
                //no_data.setVisibility(View.VISIBLE);
            } else {
                while (cursor.moveToNext()) {
                    recipe_name!!.add(cursor.getString(0))
                    recipe_calories!!.add(cursor.getString(1))
                    recipe_country!!.add(cursor.getString(2))
                    recipe_img!!.add(cursor.getString(3))
                    recipe_recipe!!.add(cursor.getString(4))
                    recipe_type!!.add(cursor.getString(5))
                    recipe_likedBy!!.add(cursor.getString(6))
                }
                //empty_imageview.setVisibility(View.GONE);
                //no_data.setVisibility(View.GONE);
            }
        }
    } //@Override
    //public boolean onCreateOptionsMenu(Menu menu) {
    //  MenuInflater inflater = getMenuInflater();
    // inflater.inflate(R.menu.my_menu, menu);
    //return super.onCreateOptionsMenu(menu);
    //}
    //@Override
    //public boolean onOptionsItemSelected(MenuItem item) {
    //  if(item.getItemId() == R.id.delete_all){
    //    confirmDialog();
    //}
    //return super.onOptionsItemSelected(item);
    //}
    //void confirmDialog(){
    //  AlertDialog.Builder builder = new AlertDialog.Builder(this);
    //builder.setTitle("Delete All?");
    //builder.setMessage("Are you sure you want to delete all Data?");
    //builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
    //@Override
    //public void onClick(DialogInterface dialogInterface, int i) {
    //  MyDatabaseHelper myDB = new MyDatabaseHelper(MainActivity.this);
    // myDB.deleteAllData();
    //Refresh Activity
    //Intent intent = new Intent(MainActivity.this, MainActivity.class);
    //startActivity(intent);
    //finish();
    //}
    //});
    //builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
    //  @Override
    //public void onClick(DialogInterface dialogInterface, int i) {
    //}
    //});
    //builder.create().show();
    //}
}