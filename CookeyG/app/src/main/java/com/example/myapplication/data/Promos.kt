package com.example.myapplication.data

data class Promos(var after: String ?= null, var before: String ?= null, var description: String ?= null, var discount: Int ?= null, var img: String ?= null, var name: String ?= null, var place: String ?= null)
