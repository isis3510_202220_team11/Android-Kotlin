package com.example.myapplication.helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast


class DatabaseHelper(private val context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = "CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_CALORIES + " TEXT, " +
                COLUMN_COUNTRY + " TEXT, " +
                COLUMN_IMG + " TEXT, " +
                COLUMN_RECIPE + " TEXT, " +
                COLUMN_TYPE + " TEXT, " +
                COLUMN_LIKEDBY + " TEXT);"
        db.execSQL(query)
        //addRecipe();
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun addRecipe() {
        deleteAllData()
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Brown Stew Chicken")
        cv.put(COLUMN_CALORIES, "334")
        cv.put(COLUMN_COUNTRY, "Colombia")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/chicken.png?alt=media&token=82962563-f07e-446c-9ba1-9d04b2c58b04"
        )
        cv.put(
            COLUMN_RECIPE,
            "Squeeze lime over chicken and rub well. Drain off excess lime juice. Combine tomato, scallion, onion, garlic, pepper, thyme, pimento and soy sauce in a large bowl with the chicken pieces. Cover and marinate at least one hour. Heat oil in a dutch pot or large saucepan. Shake off the seasonings as you remove each piece of chicken from the marinade. Reserve the marinade for sauce. Lightly brown the chicken a few pieces at a time in very hot oil. Place browned chicken pieces on a plate to rest while you brown the remaining pieces. Drain off excess oil and return the chicken to the pan. Pour the marinade over the chicken and add the carrots. Stir and cook over medium heat for 10 minutes. Mix flour and coconut milk and add to stew, stirring constantly. Turn heat down to minimum and cook another 20 minutes or until tender."
        )
        cv.put(COLUMN_TYPE, "Common")
        cv.put(COLUMN_LIKEDBY, "46")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe1()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe1() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Burek")
        cv.put(COLUMN_CALORIES, "240")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/burek.png?alt=media&token=cf552f77-50e5-4b71-ae8f-894f87165e66"
        )
        cv.put(
            COLUMN_RECIPE,
            "Fry the finely chopped onions and minced meat in oil. Add the salt and pepper. Grease a round baking tray and put a layer of pastry in it. Cover with a thin layer of filling and cover this with another layer of filo pastry which must be well coated in oil. Put another layer of filling and cover with pastry. When you have five or six layers, cover with filo pastry, bake at 200ºC/392ºF for half an hour and cut in quarters and serve."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "30")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe2()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe2() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Caesar Salad")
        cv.put(COLUMN_CALORIES, "350")
        cv.put(COLUMN_COUNTRY, "USA")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/caesarSalad.png?alt=media&token=3deac982-93f0-48a5-b32c-9a5c8b5f792a"
        )
        cv.put(
            COLUMN_RECIPE,
            "In a large mixing bowl, combine all of your ingredients and toss gently to coat the lettuce in caesar dressing. This recipe makes enough croutons for two full salads so you’ll have them ready to go for round 2!"
        )
        cv.put(COLUMN_TYPE, "Salads")
        cv.put(COLUMN_LIKEDBY, "15")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe3()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe3() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Fresh Sardines")
        cv.put(COLUMN_CALORIES, "400")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/sardines.png?alt=media&token=dd0bb52b-9db5-4366-a62b-91a795368ce6"
        )
        cv.put(
            COLUMN_RECIPE,
            "Wash the fish under the cold tap. Roll in the flour and deep fry in oil until crispy. Lay on kitchen towel to get rid of the excess oil and serve hot or cold with a slice of lemon."
        )
        cv.put(COLUMN_TYPE, "Assiatic")
        cv.put(COLUMN_LIKEDBY, "13")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe4()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe4() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Mushroom soup with buckwheat")
        cv.put(COLUMN_CALORIES, "255")
        cv.put(COLUMN_COUNTRY, "Colombia")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/mushroom.png?alt=media&token=cd07bbd3-8ba3-4bc4-8e18-a74cfca64fb8"
        )
        cv.put(
            COLUMN_RECIPE,
            "Chop the onion and garlic, slice the mushrooms and wash the buckwheat. Heat the oil and lightly sauté the onion. Add the mushrooms and the garlic and continue to sauté. Add the salt, vegetable seasoning, buckwheat and the bay leaf and cover with water. Simmer gently and just before it is completely cooked, add pepper, sour cream mixed with flour, the chopped parsley and vinegar to taste."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "237")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe5()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe5() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Shakshuka")
        cv.put(COLUMN_CALORIES, "345")
        cv.put(COLUMN_COUNTRY, "China")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/shakshuka.png?alt=media&token=c83baadc-1c55-4f04-a0a9-4b02cf27ba02"
        )
        cv.put(
            COLUMN_RECIPE,
            "Heat the oil in a frying pan that has a lid, then soften the onions, chilli, garlic and coriander stalks for 5 mins until soft. Stir in the tomatoes and sugar, then bubble for 8-10 mins until thick. Can be frozen for 1 month. Using the back of a large spoon, make 4 dips in the sauce, then crack an egg into each one. Put a lid on the pan, then cook over a low heat for 6-8 mins, until the eggs are done to your liking. Scatter with the coriander leaves and serve with crusty bread."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "87")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe6()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addRecipe6() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Smoky Lentil Chili with Squash")
        cv.put(COLUMN_CALORIES, "400")
        cv.put(COLUMN_COUNTRY, "Mexico")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/chili.png?alt=media&token=ed5fff3a-61f4-4cc7-a58b-b5d941a8c036"
        )
        cv.put(
            COLUMN_RECIPE,
            "Begin by roasting the squash. Slice it into thin crescents and drizzle with a little oil and sprinkle with sea salt. I added a fresh little sage I had in the fridge, but it’s unnecessary. Roast the squash a 205 C (400 F) for 20-30 minutes, flipping halfway through, until soft and golden. Let cool and chop into cubes. Meanwhile, rinse the lentils and cover them with water. Bring them to the boil then turn down to a simmer and let cook (uncovered) for 20-30 minutes, or until tender. Drain and set aside. While the lentils are cooking heat the 1 Tbsp. of oil on low in a medium pot. Add the onions and leeks and sauté for 5 or so minutes, or until they begin to soften. Add the garlic next along with the cumin and coriander, cooking for a few more minutes. Add the remaining spices – paprika, cinnamon, chilli, cocoa, Worcestershire sauce, salt, and oregano. Next add the can of tomatoes, the water or stock, and carrots. Let simmer, covered, for 20 minutes or until the veg is tender and the mixture has thickened up. You’ll need to check on the pot periodically for a stir and a top of of liquid if needed. Add the lentils and chopped roasted squash. Let cook for 10 more minutes to heat through. Serve with sliced jalapeno, lime wedges, cilantro, green onions, and cashew sour cream. SIMPLE CASHEW SOUR CREAM 1 Cup Raw Unsalted Cashews Pinch Sea Salt 1 tsp. Apple Cider Vinegar Water Bring some water to the boil, and use it to soak the cashews for at least four hours. Alternatively, you can use cold water and let the cashews soak overnight, but I’m forgetful/lazy, so often use the boil method which is much faster. After the cashews have soaked, drain them and add to a high speed blender. Begin to puree, slowly adding about 1/2 cup fresh water, until a creamy consistency is reached. You may need to add less or more water to reach the desired consistency. Add a pinch of sea salt and vinegar (or lemon juice)."
        )
        cv.put(COLUMN_TYPE, "Mexican")
        cv.put(COLUMN_LIKEDBY, "5")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe7()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            
        }
    }

    fun addRecipe7() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Sushi")
        cv.put(COLUMN_CALORIES, "200")
        cv.put(COLUMN_COUNTRY, "Japan")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/sushi.png?alt=media&token=142ad0fa-761d-463e-a072-a76a3da8e6a2"
        )
        cv.put(
            COLUMN_RECIPE,
            "STEP 1 TO MAKE SUSHI ROLLS: Pat out some rice. Lay a nori sheet on the mat, shiny-side down. Dip your hands in the vinegared water, then pat handfuls of rice on top in a 1cm thick layer, leaving the furthest edge from you clear. STEP 2 Spread over some Japanese mayonnaise. Use a spoon to spread out a thin layer of mayonnaise down the middle of the rice. STEP 3 Add the filling. Get your child to top the mayonnaise with a line of their favourite fillings – here we’ve used tuna and cucumber. STEP 4 Roll it up. Lift the edge of the mat over the rice, applying a little pressure to keep everything in a tight roll. STEP 5 Stick down the sides like a stamp. When you get to the edge without any rice, brush with a little water and continue to roll into a tight roll. STEP 6 Wrap in cling film. Remove the mat and roll tightly in cling film before a grown-up cuts the sushi into thick slices, then unravel the cling film. STEP 7 TO MAKE PRESSED SUSHI: Layer over some smoked salmon. Line a loaf tin with cling film, then place a thin layer of smoked salmon inside on top of the cling film. STEP 8 Cover with rice and press down. Press about 3cm of rice over the fish, fold the cling film over and press down as much as you can, using another tin if you have one. STEP 9 Tip it out like a sandcastle. Turn block of sushi onto a chopping board. Get a grown-up to cut into fingers, then remove the cling film. STEP 10 TO MAKE SUSHI BALLS: Choose your topping. Get a small square of cling film and place a topping, like half a prawn or a small piece of smoked salmon, on it. Use damp hands to roll walnut-sized balls of rice and place on the topping. STEP 11 Make into tight balls. Bring the corners of the cling film together and tighten into balls by twisting it up, then unwrap and serve."
        )
        cv.put(COLUMN_TYPE, "Assiatic")
        cv.put(COLUMN_LIKEDBY, "11")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe8()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {

        }
    }

    fun addRecipe8() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Walnut Roll Guzvara")
        cv.put(COLUMN_CALORIES, "300")
        cv.put(COLUMN_COUNTRY, "India")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/walnut.png?alt=media&token=393f6fd9-b8e1-47fd-aa42-38f950db3f60"
        )
        cv.put(
            COLUMN_RECIPE,
            "Mix all the ingredients for the dough together and knead well. Cover the dough and put to rise until doubled in size which should take about 2 hours. Knock back the dough and knead lightly. Divide the dough into two equal pieces; roll each piece into an oblong about 12 inches by 8 inches. Mix the filling ingredients together and divide between the dough, spreading over each piece. Roll up the oblongs as tightly as possible to give two 12 inch sausages. Place these side by side, touching each other, on a greased baking sheet. Cover and leave to rise for about 40 minutes. Heat oven to 200ºC (425ºF). Bake for 30-35 minutes until well risen and golden brown. Bread should sound hollow when the base is tapped. Remove from oven and brush the hot bread top with milk. Sift with a generous covering of icing sugar."
        )
        cv.put(COLUMN_TYPE, "International")
        cv.put(COLUMN_LIKEDBY, "15")
        val result = db.insert(TABLE_NAME, null, cv)
        addRecipe9()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {

        }
    }

    fun addRecipe9() {
        //deleteAllData();
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Yaki Udon")
        cv.put(COLUMN_CALORIES, "390")
        cv.put(COLUMN_COUNTRY, "India")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/yaki.png?alt=media&token=eeb9d552-123f-44b4-ad76-b9b45756e3ff"
        )
        cv.put(
            COLUMN_RECIPE,
            "Boil some water in a large saucepan. Add 250ml cold water and the udon noodles. (As they are so thick, adding cold water helps them to cook a little bit slower so the middle cooks through). If using frozen or fresh noodles, cook for 2 mins or until al dente; dried will take longer, about 5-6 mins. Drain and leave in the colander. Heat 1 tbsp of the oil, add the onion and cabbage and sauté for 5 mins until softened. Add the mushrooms and some spring onions, and sauté for 1 more min. Pour in the remaining sesame oil and the noodles. If using cold noodles, let them heat through before adding the ingredients for the sauce – otherwise tip in straight away and keep stir-frying until sticky and piping hot. Sprinkle with the remaining spring onions."
        )
        cv.put(COLUMN_TYPE, "Assiatic")
        cv.put(COLUMN_LIKEDBY, "18")
        val result = db.insert(TABLE_NAME, null, cv)
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {

        }
    }

    fun readAllData(): Cursor? {
        val query = "SELECT * FROM " + TABLE_NAME
        val db = this.readableDatabase
        var cursor: Cursor? = null
        if (db != null) {
            cursor = db.rawQuery(query, null)
        }
        return cursor
    }


    fun deleteAllData() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM " + TABLE_NAME)
    }

    companion object {
        private const val DATABASE_NAME = "Recipes.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "my_recipes"
        private const val COLUMN_CALORIES = "recipe_calories"
        private const val COLUMN_COUNTRY = "recipe_country"
        private const val COLUMN_IMG = "recipe_img"
        private const val COLUMN_LIKEDBY = "recipe_likedBy"
        private const val COLUMN_NAME = "recipe_name"
        private const val COLUMN_RECIPE = "recipe_recipe"
        private const val COLUMN_TYPE = "recipe_type"
    }
}
