package com.example.myapplication.util

sealed class RegisterValidation()
{
    object Succes: RegisterValidation()
    data class Failed (val message: String): RegisterValidation()

}

data class RegisterFieldsState(
    val firstName: RegisterValidation,
    val lastName: RegisterValidation,
    val email: RegisterValidation,
    val country: RegisterValidation,
    val password: RegisterValidation
)
