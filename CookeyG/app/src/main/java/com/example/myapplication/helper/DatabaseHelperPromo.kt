package com.example.myapplication.helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

class DatabaseHelperPromo(private val context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = "CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_BEFORE + " TEXT, " +
                COLUMN_AFTER + " TEXT, " +
                COLUMN_IMG + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_PLACE + " TEXT, " +
                COLUMN_DISCOUNT + " TEXT);"
        db.execSQL(query)
        //addRecipe();
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun addPromo() {
        deleteAllData()
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Kikes eggs")
        cv.put(COLUMN_BEFORE, "3.61 USD")
        cv.put(COLUMN_AFTER, "4.51 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/huevos.png?alt=media&token=2368914e-e362-4d45-832e-86624efe5a84"
        )
        cv.put(COLUMN_DESCRIPTION, "Egg Aa Red 30Und Pet KIKES 30 und")
        cv.put(COLUMN_PLACE, "Carulla")
        cv.put(COLUMN_DISCOUNT, "20")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo2()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo2() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Coca cola")
        cv.put(COLUMN_BEFORE, "2.38 USD")
        cv.put(COLUMN_AFTER, "2.10 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/cocacola.png?alt=media&token=73e334c1-af6b-4b02-9e8c-8c32b35770f8"
        )
        cv.put(COLUMN_DESCRIPTION, "Gaseosa duo gratis gaseosa 1.5 COCA COLA 6000 ml")
        cv.put(COLUMN_PLACE, "Exito")
        cv.put(COLUMN_DISCOUNT, "20")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo3()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo3() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Potatoes Super Ricas")
        cv.put(COLUMN_BEFORE, "2.04 USD")
        cv.put(COLUMN_AFTER, "3.14 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/potatoes.png?alt=media&token=20544388-3ad7-40fd-916c-b2c167b06640"
        )
        cv.put(COLUMN_DESCRIPTION, "Papa 12 und popular chicken super tasty 300 gr")
        cv.put(COLUMN_PLACE, "D1")
        cv.put(COLUMN_DISCOUNT, "35")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo4()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo4() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Ground coffee")
        cv.put(COLUMN_BEFORE, "2.34 USD")
        cv.put(COLUMN_AFTER, "2.64 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/cafesello.png?alt=media&token=8818e2c9-5124-40c7-91ca-78d4cca1c7af"
        )
        cv.put(COLUMN_DESCRIPTION, "Cafe Molido Sello Rojo")
        cv.put(COLUMN_PLACE, "Carulla")
        cv.put(COLUMN_DISCOUNT, "20")
        val result = db.insert(TABLE_NAME, null, cv)
        addPromo5()
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun addPromo5() {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COLUMN_NAME, "Canned tuna")
        cv.put(COLUMN_BEFORE, "0.99 USD")
        cv.put(COLUMN_AFTER, "1.82 USD")
        cv.put(
            COLUMN_IMG,
            "https://firebasestorage.googleapis.com/v0/b/cookey-login.appspot.com/o/atun.png?alt=media&token=d3811dff-01df-4a3c-a59f-8b5c5ef58d3e"
        )
        cv.put(COLUMN_DESCRIPTION, "Frescampo. Preserved In Tomato Sauce 425")
        cv.put(COLUMN_PLACE, "Exito")
        cv.put(COLUMN_DISCOUNT, "46")
        val result = db.insert(TABLE_NAME, null, cv)
        if (result == -1L) {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    fun readAllData(): Cursor? {
        val query = "SELECT * FROM " + TABLE_NAME
        val db = this.readableDatabase
        var cursor: Cursor? = null
        if (db != null) {
            cursor = db.rawQuery(query, null)
        }
        return cursor
    }


    fun deleteAllData() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM " + TABLE_NAME)
    } //DeleteVideo5Minuto9

    companion object {
        private const val DATABASE_NAME = "Promos.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "my_promos"
        private const val COLUMN_BEFORE = "promo_before"
        private const val COLUMN_AFTER = "promo_after"
        private const val COLUMN_IMG = "promo_img"
        private const val COLUMN_DESCRIPTION = "promo_description"
        private const val COLUMN_NAME = "promo_name"
        private const val COLUMN_PLACE = "promo_place"
        private const val COLUMN_DISCOUNT = "promo_discount"
    }
}