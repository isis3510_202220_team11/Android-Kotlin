package com.example.myapplication.adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.Promos
import com.example.myapplication.util.MyCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class BestDealsAdapter(private val userList : ArrayList<Promos> ) : RecyclerView.Adapter<BestDealsAdapter.MyViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int){

        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestDealsAdapter.MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_best_promos,
            parent, false)

        return MyViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: BestDealsAdapter.MyViewHolder, position: Int) {


        val userRecipe : Promos = userList[position]
        holder.name.text = userRecipe.name
        GlobalScope.launch (Dispatchers.IO){
            getImage(userRecipe.img, holder)
        }
        holder.before.text = userRecipe.before
        holder.after.text = userRecipe.after
        holder.place.text = userRecipe.place
        holder.discount.text = userRecipe.discount.toString()
        holder.description.text = userRecipe.description
    }

    private suspend fun getImage(imgURL: String ?= null, holder: MyViewHolder){

        var img1 = MyCache.instance.retrieveBitmapFromCache(imgURL!!)
        if (img1 != null) {
            withContext(Dispatchers.Main) {
                holder.img.setImageBitmap(img1)
            }
        } else {
            try {
                val url = URL(imgURL)
                img1 = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                MyCache.instance.saveBitmapToCahche(imgURL, img1)
                withContext(Dispatchers.Main) {
                    holder.img.setImageBitmap(img1)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    holder.img.setImageResource(R.drawable.noavailable)
                }
            }

        }


    }

    override fun getItemCount(): Int {

        return userList.size

    }

    public class MyViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){

        val name : TextView = itemView.findViewById(R.id.product_names)
        val img : ImageView = itemView.findViewById(R.id.img_products)
        val before : TextView = itemView.findViewById(R.id.beforetexts)
        val after : TextView = itemView.findViewById(R.id.aftertexts)
        val place : TextView = itemView.findViewById(R.id.places)
        val discount : TextView = itemView.findViewById(R.id.discounts)
        val description : TextView = itemView.findViewById(R.id.descriptions)
        //-----------------------------------
        init{
            itemView.setOnClickListener {

                listener.onItemClick(adapterPosition)

            }
        }


    }
}