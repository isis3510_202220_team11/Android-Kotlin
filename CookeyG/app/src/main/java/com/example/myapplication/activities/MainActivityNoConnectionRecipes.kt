package com.example.myapplication.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R

class MainActivityNoConnectionRecipes : AppCompatActivity() {
    //var isConnected = false
    private lateinit var checkInternet: Button
    var connectivityManager: ConnectivityManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val actionBar = supportActionBar
        actionBar!!.title = "Menu"

        checkInternet = findViewById(R.id.button2)
        checkInternet.setOnClickListener(View.OnClickListener {
            if (isConnectedTrue) {
                val i = Intent(this@MainActivityNoConnectionRecipes, RecipesActivity::class.java)
                startActivity(i)
                //finish()
                //Toast.makeText(Prueba.this,"Connected",Toast.LENGTH_SHORT).show();
            } else {
                val i = Intent(this@MainActivityNoConnectionRecipes, LocalStorageRecipes::class.java)
                startActivity(i)
                //finish()
            }
        })

        checkInternet = findViewById(R.id.button3)
        checkInternet.setOnClickListener(View.OnClickListener {
            if (isConnectedTrue) {
                val i = Intent(this@MainActivityNoConnectionRecipes, PromosActivity::class.java)
                startActivity(i)
            } else {
                val i = Intent(this@MainActivityNoConnectionRecipes, LocalStoragePromos::class.java)
                startActivity(i)
            }
        })


        checkInternet = findViewById(R.id.recommendations)
        checkInternet.setOnClickListener(View.OnClickListener {
            if (isConnectedTrue) {
                val i = Intent(this@MainActivityNoConnectionRecipes, RecommendationsActivity::class.java)
                startActivity(i)
                //finish()
                //Toast.makeText(Prueba.this,"Connected",Toast.LENGTH_SHORT).show();
            } else {
                val i = Intent(this@MainActivityNoConnectionRecipes, NoInternetRecommendations::class.java)
                startActivity(i)
                //finish()
            }
        })

        checkInternet = findViewById(R.id.recommendations2)
        checkInternet.setOnClickListener(View.OnClickListener {
            if (isConnectedTrue) {
                val i = Intent(this@MainActivityNoConnectionRecipes, LocationActivity::class.java)
                startActivity(i)
                //finish()
                //Toast.makeText(Prueba.this,"Connected",Toast.LENGTH_SHORT).show();
            } else {
                val i = Intent(this@MainActivityNoConnectionRecipes, NoInternetLocationRecommendations::class.java)
                startActivity(i)
                //finish()
            }
        })

        checkInternet = findViewById(R.id.recipesCaloriesR)
        checkInternet.setOnClickListener(View.OnClickListener {
            if (isConnectedTrue) {
                val i = Intent(this@MainActivityNoConnectionRecipes, CaloriesMainActivity::class.java)
                startActivity(i)
                //finish()
                //Toast.makeText(Prueba.this,"Connected",Toast.LENGTH_SHORT).show();
            } else {
                val i = Intent(this@MainActivityNoConnectionRecipes, LocalStorageCalories::class.java)
                startActivity(i)
                //finish()
            }
        })

        val boton3: Button = findViewById(R.id.inventorybutton)


        boton3.setOnClickListener(){
            //val intent = Intent(this, LocationActivity::class.java)
            val intent = Intent(this, InventoryActivity::class.java)
            startActivity(intent)
        }





    }

    //------------------------------------------------------------
    var isConnectedTrue = false
    var myCallBack: NetworkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            isConnectedTrue = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            isConnectedTrue = false
        }
    }

    //-----------------------------------------------------------
    private fun registerCallBack() {
        connectivityManager =
            applicationContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager!!.registerDefaultNetworkCallback(myCallBack)
        }
    }

    private fun unRegisterCallBack() {
        if (connectivityManager == null) return
        connectivityManager!!.unregisterNetworkCallback(myCallBack)
    }

    override fun onResume() {
        super.onResume()
        registerCallBack()
    }

    override fun onPause() {
        super.onPause()
        unRegisterCallBack()
    }


}