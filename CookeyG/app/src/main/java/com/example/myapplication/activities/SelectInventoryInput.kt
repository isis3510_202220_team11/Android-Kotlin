package com.example.myapplication.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.R
import com.example.myapplication.adapters.InventoryRepository
import com.example.myapplication.util.ConnectionLiveData
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import java.io.ByteArrayOutputStream

class SelectInventoryInput : AppCompatActivity(){

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    var bundle: Bundle = Bundle()
    lateinit var connectionLiveData: ConnectionLiveData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_item_input)

        val actionBar = supportActionBar
        actionBar!!.title = "Select input method"
        // actionBar.setDisplayHomeAsUpEnabled(true)

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        val manualBtn: Button = findViewById(R.id.add_manually_button)
        manualBtn.setOnClickListener {
            bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Manual input")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle)

            startActivity(Intent(this, NewItemInput::class.java))
            finish()
        }

        val hotProductBtn: Button = findViewById(R.id.add_hot_product)
        hotProductBtn.setOnClickListener {
            bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Hot product input")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle)


            val hotProductIntent = Intent(this, NewItemInput::class.java)
            hotProductIntent.putExtra("is_hot_product", true)
            startActivity(hotProductIntent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()

        val scanBtn: Button = findViewById(R.id.scan_product_button)

        scanBtn.setOnClickListener {
            bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Barcode input")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle)

            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {

                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startCamera.launch(cameraIntent)
            } else {
                ActivityCompat.requestPermissions(
                    this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
                )
            }
        }
    }

    private val startCamera = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result ->
        if(result.resultCode == RESULT_OK){
            val barcodeImg : Bitmap = result.data!!.extras!!.get("data") as Bitmap
            val intent = Intent(this, ShowBarcodeSearchResult::class.java)
            this.openFileOutput("temp.jpg", Context.MODE_PRIVATE).use{
                val bos= ByteArrayOutputStream()
                barcodeImg.compress(Bitmap.CompressFormat.JPEG,100,bos)
                val bitmapData=bos.toByteArray()
                it.write(bitmapData)
                it.close()
            }
            intent.putExtra("barcodeImg",barcodeImg)
            startActivity(intent)
            finish()
        }
    }

    companion object {
        private const val REQUEST_CODE_PERMISSIONS = 1
        private val REQUIRED_PERMISSIONS =
            mutableListOf (
                Manifest.permission.CAMERA,
            ).apply {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                    add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }
            }.toTypedArray()
    }
}